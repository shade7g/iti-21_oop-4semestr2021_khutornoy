﻿using System;
using System.Drawing;

namespace GraphicEditor_Library
{
    /// <summary>
    /// Прямоугольник.
    /// </summary>
    public class Rectangle
    {
        private int width;
        private int height;
        private Color color;

        /// <summary>
        /// Инициализатор класса Rectangle.
        /// </summary>
        /// <param name="width"> Сторона A. </param>
        /// <param name="height"> Сторона B. </param>
        /// <param name="color"> Цвет. </param>
        public Rectangle(int width, int height, Color color)
        {
            if (width <= 0)
            {
                throw new ArgumentException("Сторона A должна быть > 0 !");
            }

            if (height <= 0)
            {
                throw new ArgumentException("Сторона B должна быть > 0 !");
            }

            if (color == null)
            {
                throw new ArgumentNullException("Цвет прямоугольника не указан !");
            }

            this.width = width;
            this.height = height;
            this.color = color;
        }

        /// <summary>
        /// Сторона A.
        /// </summary>
        public int Width
        {
            get { return width; }
        }

        /// <summary>
        /// Сторона B.
        /// </summary>
        public int Height
        {
            get { return height; }
        }

        /// <summary>
        /// Цвет.
        /// </summary>
        public Color Color
        {
            get { return color; }
        }

    }
}
