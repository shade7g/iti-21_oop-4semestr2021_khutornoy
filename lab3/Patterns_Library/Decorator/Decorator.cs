﻿using System;
using TestObjects_Library;

namespace Patterns_Library
{
    /// <summary>
    /// Декоратор для объектов типа Man.
    /// </summary>
    public abstract class Decorator : Man
    {
        protected Man man;

        public Decorator(Man man) : base(man.Name, man.Age, man.Weight, man.Height)
        {
            if (man == null)
            {
                throw new ArgumentNullException("Информация о человеке не передана!");
            }

            this.man = man;
        }
    }
}
