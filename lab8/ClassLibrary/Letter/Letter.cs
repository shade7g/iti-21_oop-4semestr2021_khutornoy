﻿using ClassLibrary.Object;
using System;

namespace ClassLibrary
{
    /// <summary>
    /// Letter
    /// </summary>
    public class Letter
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int ID { get; private set; }
        /// <summary>
        /// Gets the name of the product.
        /// </summary>
        /// <value>
        /// The name of the product.
        /// </value>
        public string SubTypeName { get; private set; }
        /// <summary>
        /// Gets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        public int SubTypeID { get; private set; }
        /// <summary>
        /// Gets the name of the producer.
        /// </summary>
        /// <value>
        /// The name of the producer.
        /// </value>
        public string TypeName { get; private set; }
        /// <summary>
        /// Gets the producer identifier.
        /// </summary>
        /// <value>
        /// The producer identifier.
        /// </value>
        public int TypeID { get; private set; }
        /// <summary>
        /// Gets the identifier producer.
        /// </summary>
        /// <value>
        /// The identifier producer.
        /// </value>
        public int IDType { get; private set; }
        /// <summary>
        /// Gets the identifier product.
        /// </summary>
        /// <value>
        /// The identifier product.
        /// </value>
        public int IDSubType { get; private set; }
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; private set; }
        /// <summary>
        /// Gets the count.
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        public int Weight { get; private set; }
        /// <summary>
        /// Gets the cost.
        /// </summary>
        /// <value>
        /// The cost.
        /// </value>
        public int Quantity { get; private set; }

        /// <summary>
        /// Gets the information.
        /// </summary>
        /// <value>
        /// The information.
        /// </value>
        public string Info { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Letter"/> class.
        /// </summary>
        /// <param name="receipt">The receipt.</param>
        /// <param name="producer">The producer.</param>
        /// <param name="product">The product.</param>
        /// <param name="unit">The unit.</param>
        public Letter(Animals animal, Types type, SubTypes subtype)
        {
            ID = animal.ID;
            Name = animal.Name;
            TypeID = animal.TypeID;
            SubTypeID = animal.SubTypeID;
            Weight = animal.Weight;
            Quantity = animal.Quantity;
            IDType = type.TypeId;
            TypeName = type.Name;
            SubTypeName = subtype.Name;
            IDSubType = subtype.SubTypeId;

            Info = $"№{ID}, зверь: {Name}, Тип: {TypeName}, Подтип: {SubTypeName}, вес: {Weight},  размер популяции: {Quantity}.";
        }
    }
}