﻿using System.Collections.Generic;

namespace ClassLibrary
{
    /// <summary>
    /// AllShop
    /// </summary>
    public class AllShop
    {
        /// <summary>
        /// Gets the main shop interface.
        /// </summary>
        /// <value>
        /// The main shop interface.
        /// </value>
        public IAnimals AnimalsInterface { get; private set; }
        /// <summary>
        /// Gets the product interface.
        /// </summary>
        /// <value>
        /// The product interface.
        /// </value>
        public ITypes TypesInterface { get; private set; }
        /// <summary>
        /// Gets the producer interface.
        /// </summary>
        /// <value>
        /// The producer interface.
        /// </value>
        public ISubTypes SubTypesInterface { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AllShop"/> class.
        /// </summary>
        /// <param name="factory">The factory.</param>
        public AllShop(Factory factory)
        {
            AnimalsInterface = factory.GetAnimals();
            TypesInterface = factory.GetTypes();
            SubTypesInterface = factory.GetSubTypes();
        }

        /// <summary>
        /// Gets all records.
        /// </summary>
        /// <returns></returns>
        public List<Letter> GetAllRecords()
        {
            List<Letter> records = new List<Letter>();
            var elements = AnimalsInterface.GetNotes();
            foreach (var element in elements)
            {
                var servicez = TypesInterface.GetNote(element.TypeID);
                var standart = SubTypesInterface.GetNote(element.SubTypeID);
                records.Add(new Letter(element, servicez, standart));
            }
            return records;
        }

        /// <summary>
        /// Gets the notes.
        /// </summary>
        /// <returns></returns>
        public List<SubTypesLetter> GetNotes()
        {
            List<SubTypesLetter> records = new List<SubTypesLetter>();
            var servicezs = SubTypesInterface.GetNotes();
            foreach (var servicez in servicezs)
            {
                records.Add(new SubTypesLetter(servicez));
            }
            return records;
        }

        /// <summary>
        /// Gets the producers.
        /// </summary>
        /// <returns></returns>
        public List<TypesLetter> GetTypes()
        {
            List<TypesLetter> records = new List<TypesLetter>();
            var producers = TypesInterface.GetNotes();
            foreach (var producer in producers)
            {
                records.Add(new TypesLetter(producer));
            }
            return records;
        }
    }
}