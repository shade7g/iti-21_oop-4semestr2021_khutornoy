﻿
namespace GraphicEditor
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Main_PictureBox = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ColorChange_Button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DrawRectangle_Button = new System.Windows.Forms.Button();
            this.SideB_Box = new System.Windows.Forms.TextBox();
            this.SideA_Box = new System.Windows.Forms.TextBox();
            this.ChangeColor_Panel = new System.Windows.Forms.ColorDialog();
            this.EraserValue_TrackBar = new System.Windows.Forms.TrackBar();
            this.EraserValue_Label = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Eraser_Box = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.Main_PictureBox)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EraserValue_TrackBar)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Main_PictureBox
            // 
            this.Main_PictureBox.BackColor = System.Drawing.Color.White;
            this.Main_PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Main_PictureBox.Location = new System.Drawing.Point(22, 166);
            this.Main_PictureBox.Name = "Main_PictureBox";
            this.Main_PictureBox.Size = new System.Drawing.Size(1304, 669);
            this.Main_PictureBox.TabIndex = 0;
            this.Main_PictureBox.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SteelBlue;
            this.panel1.Controls.Add(this.ColorChange_Button);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.DrawRectangle_Button);
            this.panel1.Controls.Add(this.SideB_Box);
            this.panel1.Controls.Add(this.SideA_Box);
            this.panel1.Location = new System.Drawing.Point(37, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(467, 135);
            this.panel1.TabIndex = 1;
            // 
            // ColorChange_Button
            // 
            this.ColorChange_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ColorChange_Button.Location = new System.Drawing.Point(291, 28);
            this.ColorChange_Button.Name = "ColorChange_Button";
            this.ColorChange_Button.Size = new System.Drawing.Size(151, 32);
            this.ColorChange_Button.TabIndex = 5;
            this.ColorChange_Button.Text = "Выбрать цвет";
            this.ColorChange_Button.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(164, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Высота";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(30, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Ширина";
            // 
            // DrawRectangle_Button
            // 
            this.DrawRectangle_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DrawRectangle_Button.Location = new System.Drawing.Point(80, 77);
            this.DrawRectangle_Button.Name = "DrawRectangle_Button";
            this.DrawRectangle_Button.Size = new System.Drawing.Size(284, 43);
            this.DrawRectangle_Button.TabIndex = 2;
            this.DrawRectangle_Button.Text = "Нарисовать прямоугольник";
            this.DrawRectangle_Button.UseVisualStyleBackColor = true;
            // 
            // SideB_Box
            // 
            this.SideB_Box.Location = new System.Drawing.Point(150, 34);
            this.SideB_Box.Name = "SideB_Box";
            this.SideB_Box.Size = new System.Drawing.Size(100, 22);
            this.SideB_Box.TabIndex = 1;
            // 
            // SideA_Box
            // 
            this.SideA_Box.Location = new System.Drawing.Point(13, 34);
            this.SideA_Box.Name = "SideA_Box";
            this.SideA_Box.Size = new System.Drawing.Size(100, 22);
            this.SideA_Box.TabIndex = 0;
            // 
            // EraserValue_TrackBar
            // 
            this.EraserValue_TrackBar.Location = new System.Drawing.Point(0, 79);
            this.EraserValue_TrackBar.Name = "EraserValue_TrackBar";
            this.EraserValue_TrackBar.Size = new System.Drawing.Size(293, 56);
            this.EraserValue_TrackBar.TabIndex = 0;
            // 
            // EraserValue_Label
            // 
            this.EraserValue_Label.AutoSize = true;
            this.EraserValue_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EraserValue_Label.Location = new System.Drawing.Point(64, 56);
            this.EraserValue_Label.Name = "EraserValue_Label";
            this.EraserValue_Label.Size = new System.Drawing.Size(183, 20);
            this.EraserValue_Label.TabIndex = 1;
            this.EraserValue_Label.Text = "Текущее значение: 0";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SteelBlue;
            this.panel2.Controls.Add(this.Eraser_Box);
            this.panel2.Controls.Add(this.EraserValue_Label);
            this.panel2.Controls.Add(this.EraserValue_TrackBar);
            this.panel2.Location = new System.Drawing.Point(644, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(299, 135);
            this.panel2.TabIndex = 2;
            // 
            // Eraser_Box
            // 
            this.Eraser_Box.AutoSize = true;
            this.Eraser_Box.CheckAlign = System.Drawing.ContentAlignment.TopRight;
            this.Eraser_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Eraser_Box.Location = new System.Drawing.Point(99, 13);
            this.Eraser_Box.Name = "Eraser_Box";
            this.Eraser_Box.Size = new System.Drawing.Size(91, 24);
            this.Eraser_Box.TabIndex = 2;
            this.Eraser_Box.Text = "Ластик";
            this.Eraser_Box.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1357, 847);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Main_PictureBox);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.Main_PictureBox)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EraserValue_TrackBar)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox Main_PictureBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button DrawRectangle_Button;
        private System.Windows.Forms.TextBox SideB_Box;
        private System.Windows.Forms.TextBox SideA_Box;
        private System.Windows.Forms.ColorDialog ChangeColor_Panel;
        private System.Windows.Forms.Button ColorChange_Button;
        private System.Windows.Forms.TrackBar EraserValue_TrackBar;
        private System.Windows.Forms.Label EraserValue_Label;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox Eraser_Box;
    }
}

