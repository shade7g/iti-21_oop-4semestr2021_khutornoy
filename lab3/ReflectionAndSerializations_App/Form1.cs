﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Reflection_Library;
using TestObjects_Library;

namespace ReflectionAndSerializations_App
{
    public partial class Form1 : Form
    {
        private string path = @"C:\Users\Max\source\repos\iti-21_oop-4semestr2021_khutornoy\lab3\";
        private readonly string[] reflectionType = new string[] {"методы", "свойства", "поля"};

        public Form1()
        {
            InitializeComponent();

            ReflectionTypeBox.Items.AddRange(reflectionType);
        }

        private void ReflectionTypeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var list = GetReflection();

            var outText = TextProcessing.GetSortedText(list);

            ReflectionOutLabel.Text = outText.ToString();
        }

        /// <summary>
        /// Получение информации о заданных элементов класса.
        /// </summary>
        /// <returns> Лист с элементами класса. </returns>
        private List<string> GetReflection()
        {
            string type = ReflectionTypeBox.SelectedItem.ToString();

            var reflection = new InfoAboutClass(new Man().GetType());

            var outText = new List<string>();

            if (type == reflectionType[0])
            {
                outText = reflection.GetMethods();
            }
            else if (type == reflectionType[1])
            {
                outText = reflection.GetProperty();
            }
            else if (type == reflectionType[2])
            {
                outText = reflection.GetFields();
            }

            return outText;
        }

        /// <summary>
        /// Валидация данных и последующая сериализация.
        /// </summary>
        private void GetSerialization_Button_Click(object sender, EventArgs e)
        {
            bool isData = true;
            int age, weight, height;

            try
            {
                if (!int.TryParse(AgeBox.Text, out age))
                {
                    isData = false;
                }

                if (!int.TryParse(WeightBox.Text, out weight))
                {
                    isData = false;
                }

                if (!int.TryParse(HeightBox.Text, out height))
                {
                    isData = false;
                }

                if (isData)
                {
                    var man = new Man(NameBox.Text, age, weight, height);
                    var xs = new XmlSerializator(path, man.GetType().Name);
                    xs.Serialize(man);
                    MessageBox.Show("Объект сериализован!");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }          
        }

        /// <summary>
        /// Десериализация и вывод данных.
        /// </summary>
        private void GetDeserialization_Button_Click(object sender, EventArgs e)
        {
            try
            {
                var man = new Man();
                var xd = new XmlDeserializator(path, man.GetType().Name);

                var obj = xd.Deserialize(man);

                if (obj is Man)
                {
                    var newMan = (Man)obj;
                    Serialization_label.Text = String.Empty;
                    Serialization_label.Text = newMan.ToString();
                }
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show(ex.Message + "\nПопробуйте сперва сериализовать объект!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
     

   