﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Animals.Tests
{
    /// <summary>
    /// 
    /// </summary>
    [TestClass()]
    public class ParserTests
    {
        /// <summary>
        /// Adds the test.
        /// </summary>
        [TestMethod()]
        public void AddTest()
        {
            string _path = @"C:\Users\Max\source\repos\iti-21_oop-4semestr2021_khutornoy\lab1\AnimalShowcase\bin\Debug\planet.xml";
            Animal animal = new Animal("betty", TypeOfAnimal.bird, "crows", 1, 789456);
            Planet planet = Parser.PlanetParser(_path);
            int countOfAnimals = planet.Length;
            Parser.Add(_path, animal, _path);
            planet = Parser.PlanetParser(_path);
            Assert.AreEqual(planet.Length, countOfAnimals + 1);
        }

        /// <summary>
        /// Deletes the test.
        /// </summary>
        [TestMethod()]
        public void DeleteTest()
        {
            string _path = @"C:\Users\Max\source\repos\iti-21_oop-4semestr2021_khutornoy\lab1\AnimalShowcase\bin\Debug\planet.xml";
            Planet planet = Parser.PlanetParser(_path);
            int countOfAnimals = planet.Length;
            Parser.Delete(_path, planet.Animals.ToArray());
            planet = Parser.PlanetParser(_path);
            Assert.AreEqual(planet.Length, countOfAnimals - 1);
        }
    }
}