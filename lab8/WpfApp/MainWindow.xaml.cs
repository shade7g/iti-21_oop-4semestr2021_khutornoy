﻿using ClassLibrary;
using ClassLibrary.Object;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace WpfApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    internal enum ViewMode
    {
        /// <summary>
        /// The main shop
        /// </summary>
        Animals,
        /// <summary>
        /// The product
        /// </summary>
        SubTypes,
        /// <summary>
        /// The producer
        /// </summary>
        Types
    }

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Windows.Window" />
    /// <seealso cref="System.Windows.Markup.IComponentConnector" />
    public partial class MainWindow : Window
    {
        /// <summary>
        /// The path connection string
        /// </summary>
        private string PATH_CONNECTION_STRING = @"Data Source=.\SQLEXPRESS;Initial Catalog=LivingPlanet;Integrated Security=True";
        /// <summary>
        /// The accounting
        /// </summary>
        private AllShop accounting;
        /// <summary>
        /// The is database loaded
        /// </summary>
        private bool IsDbLoaded = false;
        /// <summary>
        /// The view mode
        /// </summary>
        private ViewMode viewMode;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow() => InitializeComponent();

        /// <summary>
        /// Shows all records.
        /// </summary>
        private void ShowAllRecords()
        {
            if (!IsDbLoaded) return;
            ViewOfRecords.Items.Clear();
            switch (viewMode)
            {
                case ViewMode.Animals:
                    List<Letter> recordServicezs = accounting.GetAllRecords();
                    foreach (Letter record in recordServicezs)
                    {
                        ViewOfRecords.Items.Add(record);
                    }
                    break;

                case ViewMode.SubTypes:
                    List<SubTypesLetter> recordServicezs1 = accounting.GetNotes();
                    foreach (SubTypesLetter record in recordServicezs1)
                    {
                        ViewOfRecords.Items.Add(record);
                    }
                    break;

                case ViewMode.Types:
                    List<TypesLetter> recordServicezs2 = accounting.GetTypes();
                    foreach (TypesLetter record in recordServicezs2)
                    {
                        ViewOfRecords.Items.Add(record);
                    }
                    break;
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event of the ViewOfRecords control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void ViewOfRecords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        /// <summary>
        /// Loads this instance.
        /// </summary>
        private void Load()
        {
            if (!IsDbLoaded) return;
            SubTypeComboBox.Items.Clear();
            TypeComboBox.Items.Clear();

            var subTypes = accounting.SubTypesInterface.GetNotes();
            foreach (var subType in subTypes)
            {
                SubTypeComboBox.Items.Add(subType);
            }

            SubTypeComboBox.SelectedIndex = 0;

            var types = accounting.TypesInterface.GetNotes();
            foreach (var type in types)
            {
                TypeComboBox.Items.Add(type);
            }
            NameInputTextBox.Text = "Чёрт";
            TypeComboBox.SelectedIndex = 0;
        }

        /// <summary>
        /// Connects to database.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ConnectToDatabase(object sender, RoutedEventArgs e)
        {
            Factory factory = Factory.GetFactory(PATH_CONNECTION_STRING);
            accounting = new AllShop(factory);

            accounting.AnimalsInterface.GetNotes();

            IsDbLoaded = true;

            ShowAllRecords();
            Load();
        }

        /// <summary>
        /// Shops the window.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void AnimalsWindow(object sender, RoutedEventArgs e)
        {
            viewMode = ViewMode.Animals;
            ShowAllRecords();
        }

        /// <summary>
        /// Products the window.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void SubTypeWindow(object sender, RoutedEventArgs e)
        {
            viewMode = ViewMode.SubTypes;
            ShowAllRecords();
        }

        /// <summary>
        /// Adds the note.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void AddNote(object sender, RoutedEventArgs e)
        {
            if (!IsDbLoaded)
            {
                MessageBox.Show("загрузите базу данных");
                return;
            }

            if (viewMode != ViewMode.Animals)
            {
                MessageBox.Show("это не зверь");
                return;
            }

            if (NameInputTextBox.Text == "")
            {
                MessageBox.Show("неверные данные");
                return;
            }

            Animals mainShop = new Animals()
            {
                Name = NameInputTextBox.Text,
                SubTypeID = (SubTypeComboBox.SelectedItem as SubTypes).SubTypeId,
                TypeID = (TypeComboBox.SelectedItem as Types).TypeId,
                Weight = Convert.ToInt32(CostTextBox.Text),
                Quantity = Convert.ToInt32(CountTextBox.Text)
            };
            accounting.AnimalsInterface.Insert(mainShop);

            ShowAllRecords();
        }

        /// <summary>
        /// Deletes the note.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void DeleteNote(object sender, RoutedEventArgs e)
        {
            if (!IsDbLoaded)
            {
                MessageBox.Show("загрузите базу данных");
                return;
            }

            if (ViewOfRecords.SelectedValue == null)
            {
                MessageBox.Show("нужно выбрать строку");
                return;
            }

            Animals mainShop = accounting.AnimalsInterface.GetNote((ViewOfRecords.SelectedValue as Letter).ID);
            accounting.AnimalsInterface.Delete(mainShop);
            ShowAllRecords();
        }

        /// <summary>
        /// Edits the note.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void EditNote(object sender, RoutedEventArgs e)
        {
            if (!IsDbLoaded)
            {
                MessageBox.Show("загрузите базу данных");
                return;
            }

            if (NameInputTextBox.Text == "")
            {
                MessageBox.Show("неверные данные");
                return;
            }

            if (ViewOfRecords.SelectedValue == null)
            {
                MessageBox.Show("нужно выбрать строку");
                return;
            }

            Animals newMainShop = new Animals()
            {
                Name = NameInputTextBox.Name,
                SubTypeID = (SubTypeComboBox.SelectedItem as SubTypes).SubTypeId,
                TypeID = (TypeComboBox.SelectedItem as Types).TypeId,
                Weight = Convert.ToInt32(CostTextBox.Text),
                Quantity = Convert.ToInt32(CountTextBox.Text)
            };

            Animals oldMainShop = accounting.AnimalsInterface.GetNote((ViewOfRecords.SelectedValue as Letter).ID);
            accounting.AnimalsInterface.Update(oldMainShop, newMainShop);
            ShowAllRecords();
        }

        /// <summary>
        /// Adds the product.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void AddProduct(object sender, RoutedEventArgs e)
        {
            if (!IsDbLoaded)
            {
                MessageBox.Show("загрузите базу данных");
                return;
            }

            if (viewMode != ViewMode.SubTypes)
            {
                MessageBox.Show("это не подвид");
                return;
            }

            if (ProductName.Text == "")
            {
                MessageBox.Show("неверные данные");
                return;
            }

            SubTypes subType = new SubTypes()
            {
                Name = ProductName.Text
            };
            accounting.SubTypesInterface.Insert(subType);
            ShowAllRecords();
        }

        /// <summary>
        /// Edits the product.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void EditProduct(object sender, RoutedEventArgs e)
        {
            if (!IsDbLoaded)
            {
                MessageBox.Show("загрузите базу данных");
                return;
            }
            if (viewMode != ViewMode.SubTypes)
            {
                MessageBox.Show("не подвид");
                return;
            }
            if (ProductName.Text == "")
            {
                MessageBox.Show("неверные данные");
                return;
            }
            if (ViewOfRecords.SelectedValue == null)
            {
                MessageBox.Show("нужно выбрать строку");
                return;
            }
            SubTypes subType = new SubTypes()
            {
                Name = ProductName.Text
            };
            SubTypes oldProduct = accounting.SubTypesInterface.GetNote((ViewOfRecords.SelectedValue as SubTypesLetter).SubTypesID);
            accounting.SubTypesInterface.Update(oldProduct, subType);
            ShowAllRecords();
        }

        /// <summary>
        /// Deletes the product.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void DeleteProduct(object sender, RoutedEventArgs e)
        {
            if (!IsDbLoaded)
            {
                MessageBox.Show("загрузите базу данных");
                return;
            }
            if (viewMode != ViewMode.SubTypes)
            {
                MessageBox.Show("не подвид");
                return;
            }
            if (ViewOfRecords.SelectedValue == null)
            {
                MessageBox.Show("нужно выбрать строку");
                return;
            }
            SubTypes subType = accounting.SubTypesInterface.GetNote((ViewOfRecords.SelectedValue as SubTypesLetter).SubTypesID);
            accounting.SubTypesInterface.Delete(subType);
            ShowAllRecords();
        }

        /// <summary>
        /// Selections the changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        /// <summary>
        /// Handles the Click event of the Button control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            viewMode = ViewMode.Types;
            ShowAllRecords();
        }

        /// <summary>
        /// Handles the ValueChanged event of the Cost control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedPropertyChangedEventArgs{System.Double}"/> instance containing the event data.</param>
        private void Cost_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) => CostTextBox.Text = Cost.Value.ToString();

        /// <summary>
        /// Adds the producer.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void AddProducer(object sender, RoutedEventArgs e)
        {
            if (!IsDbLoaded)
            {
                MessageBox.Show("загрузите базу данных");
                return;
            }

            if (viewMode != ViewMode.Types)
            {
                MessageBox.Show("это не вид");
                return;
            }

            if (ProdcerName.Text == "")
            {
                MessageBox.Show("неверные данные");
                return;
            }

            Types producer = new Types()
            {
                Name = ProdcerName.Text
            };
            accounting.TypesInterface.Insert(producer);

            ShowAllRecords();
        }

        /// <summary>
        /// Edits the producer.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void EditProducer(object sender, RoutedEventArgs e)
        {
            if (!IsDbLoaded)
            {
                MessageBox.Show("загрузите базу данных");
                return;
            }

            if (viewMode != ViewMode.Types)
            {
                MessageBox.Show("не вид");
                return;
            }

            if (ProdcerName.Text == "")
            {
                MessageBox.Show("неверные данные");
                return;
            }

            if (ViewOfRecords.SelectedValue == null)
            {
                MessageBox.Show("нужно выбрать строку");
                return;
            }

            Types producer = new Types()
            {
                Name = ProdcerName.Text
            };

            Types oldProducer = accounting.TypesInterface.GetNote((ViewOfRecords.SelectedValue as TypesLetter).IDType);

            accounting.TypesInterface.Update(oldProducer, producer);

            ShowAllRecords();
        }

        /// <summary>
        /// Deletes the producer.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void DeleteProducer(object sender, RoutedEventArgs e)
        {
            if (!IsDbLoaded)
            {
                MessageBox.Show("загрузите базу данных");
                return;
            }

            if (viewMode != ViewMode.Types)
            {
                MessageBox.Show("не вид");
                return;
            }

            if (ViewOfRecords.SelectedValue == null)
            {
                MessageBox.Show("нужно выбрать строку");
                return;
            }

            Types producer = accounting.TypesInterface.GetNote((ViewOfRecords.SelectedValue as TypesLetter).IDType);
            accounting.TypesInterface.Delete(producer);
            ShowAllRecords();
        }
    }
}