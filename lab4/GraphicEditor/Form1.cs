﻿using System;
using System.Drawing;
using System.Windows.Forms;
using GraphicEditor_Library;

namespace GraphicEditor
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Рисунок.
        /// </summary>
        Bitmap bitmap;

        /// <summary>
        /// Значение зажата ли мышка.
        /// </summary>
        bool isMouse = false;

        /// <summary>
        /// Значение включён ли ластик.
        /// </summary>
        bool isEraser = false;

        /// <summary>
        /// Инициализатор формы.
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            bitmap = new Bitmap(Main_PictureBox.Width, Main_PictureBox.Height);
            ChangeColor_Panel.FullOpen = true;
            ChangeColor_Panel.Color = Color.Black;
            EraserValue_TrackBar.Maximum = 100;

            // Добавляем события к элементам управления через лямбды-выражений.
            ColorChange_Button.Click += (sender, e) => ColorChange_Click(sender, e);
            DrawRectangle_Button.Click += (sender, e) => DrawRectangle_Click(sender, e);

            Main_PictureBox.MouseDown += (sender, e) => PictureBox_MouseDown(sender, e);
            Main_PictureBox.MouseUp += (sender, e) => PictureBox_MouseUp(sender, e);
            Main_PictureBox.MouseMove += (sender, e) => PictureBox_MouseMove(sender, e);

            EraserValue_TrackBar.Scroll += (sender, e) => TrackBar_Scroll(sender, e);
            Eraser_Box.CheckedChanged += (sender, e) => EraserBox_CheckedChanged(sender, e);

        }

        /// <summary>
        /// Валидация данных прямоуголника и его отрисовка.
        /// </summary>
        private void DrawRectangle_Click(object sender, EventArgs e)
        {
            int a, b;

            if (int.TryParse(SideA_Box.Text, out a) && int.TryParse(SideB_Box.Text, out b))
            {
                var rectangle = new GraphicEditor_Library.Rectangle(a, b, ChangeColor_Panel.Color);
                var center = new Point(Main_PictureBox.Width / 2, Main_PictureBox.Height / 2);

                Main_PictureBox.Image = DrawingRectangle.Draw(rectangle, center, bitmap);
            }                     
        }

        /// <summary>
        /// Открыть панель с изменением цвета.
        /// </summary>
        private void ColorChange_Click(object sender, EventArgs e)
        {            
            ChangeColor_Panel.ShowDialog();
        }     
     
        /// <summary>
        /// Если мышка зажата и выбран ластик, то ластик стирает.
        /// </summary>
        private void PictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouse && isEraser)
            {
                using (Graphics g = Main_PictureBox.CreateGraphics())
                {
                    Brush brush = new SolidBrush(Main_PictureBox.BackColor);

                    g.FillRectangle(brush, e.X, e.Y, EraserValue_TrackBar.Value, EraserValue_TrackBar.Value);

                    brush.Dispose();
                }
            }
        }

        /// <summary>
        /// Отслеживание мышки после нажатия.
        /// </summary>
        private void PictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            isMouse = true;
        }

        /// <summary>
        /// Отслеживание мышки после отпускания.
        /// </summary>
        private void PictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            isMouse = false;
        }

        /// <summary>
        /// Вывод значения толщины ластика.
        /// </summary>
        private void TrackBar_Scroll(object sender, EventArgs e)
        {
            EraserValue_Label.Text = $"Текущее значение: {EraserValue_TrackBar.Value}";
        }

        /// <summary>
        /// Получаем значение о активности ластика (включён или нет).
        /// </summary>
        private void EraserBox_CheckedChanged(object sender, EventArgs e)
        {
            isEraser = Eraser_Box.Checked;
        }
    }
}
