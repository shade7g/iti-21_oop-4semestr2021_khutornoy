﻿namespace Animals
{
    /// <summary>
    /// 
    /// </summary>
    public enum TypeOfAnimal
    {
        /// <summary>
        /// The bird
        /// </summary>
        bird,
        /// <summary>
        /// The fish
        /// </summary>
        fish,
        /// <summary>
        /// The cat
        /// </summary>
        cat,
        /// <summary>
        /// The dog
        /// </summary>
        dog,
        /// <summary>
        /// The mammal
        /// </summary>
        mammal,
        /// <summary>
        /// The bunny
        /// </summary>
        bunny,
        /// <summary>
        /// The rabbit
        /// </summary>
        rabbit
    }

}