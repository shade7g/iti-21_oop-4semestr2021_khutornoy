﻿namespace ClassLibrary
{
    /// <summary>
    /// Factory
    /// </summary>
    public abstract class Factory
    {
        /// <summary>
        /// Gets the factory.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns></returns>
        public static Factory GetFactory(string connectionString)
        {
            return new FactoryShop(connectionString);
        }

        /// <summary>
        /// Gets the main shop.
        /// </summary>
        /// <returns></returns>
        public abstract IAnimals GetAnimals();

        /// <summary>
        /// Gets the producer.
        /// </summary>
        /// <returns></returns>
        public abstract ITypes GetTypes();

        /// <summary>
        /// Gets the product.
        /// </summary>
        /// <returns></returns>
        public abstract ISubTypes GetSubTypes();
    }
}