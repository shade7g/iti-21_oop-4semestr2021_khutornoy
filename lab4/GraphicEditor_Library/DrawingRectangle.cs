﻿using System.Drawing;

namespace GraphicEditor_Library
{
    /// <summary>
    /// Отрисовка прямоуголника.
    /// </summary>
    public class DrawingRectangle
    {
        /// <summary>
        /// Отрисовка прямоугольника.
        /// </summary>
        /// <param name="rectangle"> Прямоугольник. </param>
        /// <param name="center"> Координаты центра. </param>
        /// <param name="bitmap"> Рисунок. </param>
        /// <returns> Рисунок с отрисованными прямоугольником. </returns>
        public static Bitmap Draw(Rectangle rectangle, Point center, Bitmap bitmap)
        {
            var graphics = Graphics.FromImage(bitmap);

            graphics.FillRectangle(new SolidBrush(rectangle.Color), center.X - rectangle.Width / 2, center.Y - rectangle.Height / 2, rectangle.Width, rectangle.Height);

            return bitmap;
        }
    }
}
