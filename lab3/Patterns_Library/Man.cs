﻿using System;

namespace Patterns_Library
{
    /// <summary>
    /// Класс для примера сериализации и реализации патернов.
    /// </summary>
    [Serializable]
    public class Man
    {
        public string Name { get; set; }

        public int Age { get; set; }

        public float Weight { get; set; }

        public float Height { get; set; }

        public Man() { }

        public Man(string name, int age, float weight, float height)
        {
            Name = name;
            Age = age;
            Weight = weight;
            Height = height;
        }

        public override string ToString()
        {
            return $"Name - {Name}; \n Age - {Age}; \n Weight - {Weight}; \n Height - {Height}. ";
        }
    }
}
