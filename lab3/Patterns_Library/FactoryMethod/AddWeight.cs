﻿using TestObjects_Library;

namespace Patterns_Library
{
    /// <summary>
    /// Получаем декорированный объект класса Man с изменённым весом.
    /// </summary>
    public class AddWeight : ManFactory
    {
        public override Man Activation(Man man)
        {
            return new WeightChange(man, 1);
        }
    }
}
