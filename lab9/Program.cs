﻿using System;
using System.IO;
using System.Linq;

namespace Program
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(string.Join("\n", new StreamReader("КиШ.txt").ReadToEnd().Split(new char[] { ' ', '\r', '\n', ',', ';', '.', '!', '?' }).Where(word => word != "").Select(word => string.Concat(word.Where(char.IsLetter).ToArray())).Select(word => word.ToLower()).Where(word => word.Length >= 4).Distinct().ToArray()));
            Console.ReadKey();
        }
    }
}
