﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Animals.Tests
{
    /// <summary>
    /// 
    /// </summary>
    [TestClass()]
    public class PlanetTests
    {
        /// <summary>
        /// Stores the test.
        /// </summary>
        [TestMethod()]
        public void PlanetTest()
        {
            Planet planet = new Planet();
            Assert.IsNotNull(planet);

        }

        /// <summary>
        /// Stores the test1.
        /// </summary>
        [TestMethod()]
        public void PlanetTest1()
        {
            Planet planet = new Planet("Earth");
            Assert.IsNotNull(planet);
        }

        /// <summary>
        /// Converts to stringtest.
        /// </summary>
        [TestMethod()]
        public void ToStringTest()
        {
            Planet planet = new Planet("Earth");
            Assert.IsTrue("Earth Quantity of animals 0" == planet.ToString());
        }
    }
}
