﻿using ClassLibrary.Object;

namespace ClassLibrary
{
    /// <summary>
    /// ProductLetter
    /// </summary>
    public class SubTypesLetter
    {
        /// <summary>
        /// Gets the name of the product.
        /// </summary>
        /// <value>
        /// The name of the product.
        /// </value>
        public string SubTypesName { get; private set; }
        /// <summary>
        /// Gets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        public int SubTypesID { get; private set; }
        /// <summary>
        /// Gets the information.
        /// </summary>
        /// <value>
        /// The information.
        /// </value>
        public string Info { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductLetter"/> class.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <param name="unit">The unit.</param>
        public SubTypesLetter(SubTypes product)
        {
            SubTypesName = product.Name;
            SubTypesID = product.SubTypeId;

            Info = $"№{SubTypesID}, подвид: {SubTypesName}.";
        }
    }
}