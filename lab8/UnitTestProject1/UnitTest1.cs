﻿using ClassLibrary;
using ClassLibrary.Object;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryTests1
{
    [TestClass]
    public class Test
    {
        private string PATH_CONNECTION_STRING = @"Data Source=.\SQLEXPRESS;Initial Catalog=LivingPlanet;Integrated Security=True";
        private Animals animals;
        private AllShop accounting;
        private bool IsDbLoaded = false;

        [TestInitialize]
        public void Initialize()
        {
            Factory factory = Factory.GetFactory(PATH_CONNECTION_STRING);
            accounting = new AllShop(factory);

            animals = new Animals()
            {
                Name = "Чёрт",
                SubTypeID = 1,
                TypeID = 1,
                Weight = 1,
                Quantity = 1
            };
        }

        [TestMethod]
        public void TestInsert()
        {
            try
            {
                accounting.AnimalsInterface.Insert(animals);
                Assert.AreEqual(8, accounting.AnimalsInterface.GetNotes().Length);
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
            }
        }

        [TestMethod]
        public void TestUpdate()
        {
            try
            {
                Animals newAnimals = new Animals()
                {
                    Name = "Чёрт",
                    SubTypeID = 1,
                    TypeID = 1,
                    Weight = 1,
                    Quantity = 1
                };

                accounting.AnimalsInterface.Update(animals, newAnimals);
                animals = newAnimals;
                Assert.AreEqual(8, accounting.AnimalsInterface.GetNotes().Length);
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
            }
        }


        [TestMethod]
        public void TestDelete()
        {
            try
            {
                accounting.AnimalsInterface.Delete(animals);
                Assert.AreEqual(7, accounting.AnimalsInterface.GetNotes().Length);
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
            }
        }
    }
}
