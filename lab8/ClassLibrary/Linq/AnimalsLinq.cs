﻿using ClassLibrary.Object;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

namespace ClassLibrary
{
    /// <summary>
    /// MainShopLinq
    /// </summary>
    /// <seealso cref="ClassLibrary.IMainShop" />
    public class AnimalsLinq : IAnimals
    {
        /// <summary>
        /// The data context
        /// </summary>
        private DataContext _dataContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainShopLinq"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public AnimalsLinq(string connectionString)
        {
            _dataContext = new DataContext(connectionString);
        }

        /// <summary>
        /// Gets the notes.
        /// </summary>
        /// <returns></returns>
        public Animals[] GetNotes()
        {
            var objectList = from i
                             in _dataContext.GetTable<Animals>()
                             select i;
            List<Animals> elements = new List<Animals>();
            foreach (Animals element in objectList)
            {
                elements.Add(element);
            }
            return elements.ToArray();
        }

        /// <summary>
        /// Gets the note.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Animals GetNote(int id)
        {
            var objectList = from i
                             in _dataContext.GetTable<Animals>()
                             where i.ID == id
                             select i;
            return objectList.First();
        }

        /// <summary>
        /// Gets the note.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        public Animals GetNote(Animals element) => GetNote(element.ID);


        /// <summary>
        /// Inserts the specified add element.
        /// </summary>
        /// <param name="addElement">The add element.</param>
        /// <returns></returns>
        public bool Insert(Animals addElement)
        {
            _dataContext.GetTable<Animals>().InsertOnSubmit(addElement);
            _dataContext.SubmitChanges();
            var elements = GetNotes();
            addElement.ID = elements[elements.Length - 1].ID;
            return true;
        }

        /// <summary>
        /// Updates the specified edit.
        /// </summary>
        /// <param name="edit">The edit.</param>
        /// <param name="newElement">The new element.</param>
        /// <returns></returns>
        public bool Update(Animals edit, Animals newElement)
        {
            _dataContext.GetTable<Animals>().DeleteOnSubmit(edit);
            _dataContext.GetTable<Animals>().InsertOnSubmit(newElement);
            _dataContext.SubmitChanges();
            var elements = GetNotes();
            newElement.ID = elements[elements.Length - 1].ID;
            return true;
        }

        /// <summary>
        /// Deletes the specified delete.
        /// </summary>
        /// <param name="delete">The delete.</param>
        /// <returns></returns>
        public bool Delete(Animals delete)
        {
            _dataContext.GetTable<Animals>().DeleteOnSubmit(delete);
            _dataContext.SubmitChanges();
            return true;
        }
    }
}