﻿using System;
using System.Diagnostics;

namespace StreamDecoration_Library
{
    /// <summary>
    /// Класс считывающий время работы с потоком.
    /// </summary>
    public class RunTimeStream
    {
        private Stopwatch stopWatch;

        /// <summary>
        /// Инициализатор класса RunTimeStream;
        /// </summary>
        public RunTimeStream()
        {
            stopWatch = new Stopwatch();
        }
        
        /// <summary>
        /// Начало работы с потоком.
        /// </summary>
        public void Start()
        {
            stopWatch.Start();
        }

        /// <summary>
        /// Конец работы с потоком.
        /// </summary>
        /// <returns> Время работы с потоком в секундах. </returns>
        public double Stop()
        {
            stopWatch.Stop();

            TimeSpan ts = stopWatch.Elapsed;

            return ts.TotalMilliseconds; 
        }
    }
}
