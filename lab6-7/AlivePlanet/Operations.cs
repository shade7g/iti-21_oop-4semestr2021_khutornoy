﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Media
{
    public class Operations:Connection
    {
        public string[] GetKinds()
        {
            using (SqlConnection connection = GetConnection())
            {
                connection.Open();
                string sql = "Select Kind from Kinds";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();
                List<string> names = new List<string>();
                while (reader.Read())
                {
                    names.Add(reader.GetString(0));
                }
                string[] namesMas = new string[names.Count];
                for(int i=0; i < names.Count; i++)
                {
                    namesMas[i] = names[i];
                }
                return namesMas;
            }
        }

        public string[] GetSubspecies(string selectedKind)
        {
            using (SqlConnection connection = GetConnection())
            {
                connection.Open();
                string sql = "Select Subspecies from SubSpecies where Kind = '"+ selectedKind + "'";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();
                List<string> names = new List<string>();
                while (reader.Read())
                {
                    names.Add(reader.GetString(0));
                }
                string[] namesMas = new string[names.Count];
                for (int i = 0; i < names.Count; i++)
                {
                    namesMas[i] = names[i];
                }
                return namesMas;
            }
        }

        public string[] GetGetAnimalsNames()
        {
            using (SqlConnection connection = GetConnection())
            {
                connection.Open();
                string sql = "Select AnimalName from Animal";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();
                List<string> names = new List<string>();
                while (reader.Read())
                {
                    names.Add(reader.GetString(0));
                }
                string[] namesMas = new string[names.Count];
                for (int i = 0; i < names.Count; i++)
                {
                    namesMas[i] = names[i];
                }
                return namesMas;
            }
        }

        public List<string[]> GetAnimals()
        {
            using (SqlConnection connection = GetConnection())
            {
                connection.Open();
                string sql = "Select * from Animal";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();
                List<string[]> names = new List<string[]>();
                while (reader.Read())
                {
                    names.Add(new string[5]);
                    names[names.Count - 1][0] = reader[0].ToString();
                    names[names.Count - 1][1] = reader[1].ToString();
                    names[names.Count - 1][2] = reader[2].ToString();
                    names[names.Count - 1][3] = reader[3].ToString();
                    names[names.Count - 1][4] = reader[4].ToString();
                }
                
                return names;
            }
        }

        public string AddAnimal(string newName, string newKind, string newSubspecies, string newWeight, string newPopulationSize )
        {
            using (SqlConnection connection = GetConnection())
            {
                connection.Open();
                string sql = "Insert Animal Values('" + newName + "','" + newKind + "','" + newSubspecies + "','" + newWeight +
                    "','" + newPopulationSize +"')";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();
                string str = "Строка добавлена";

                return str;
            }
        }

        public string UpdateAnimal(string newName, string newKind, string newSubspecies, string newWeight, string newPopulationSize)
        {
            using (SqlConnection connection = GetConnection())
            {
                connection.Open();
                string sql = "Update Animal Set Kind = '" + newKind + "', Subspecies = '" + newSubspecies
                    + "', Weight = '" + newWeight + "', PopulationSize = '" + newPopulationSize + "' where AnimalName = '" + newName + "'";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();
                string str = "Строка обновлена";

                return str;
            }
        }

        public string RemoveAnimal(string name)
        {
            using (SqlConnection connection = GetConnection())
            {
                connection.Open();
                string sql = "Delete from Animal where AnimalName = '" + name+"'";
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();
                string str = "Строка удалена";

                return str;
            }
        }
    }
}
