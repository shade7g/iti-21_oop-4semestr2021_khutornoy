﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Animals.Tests
{
    /// <summary>
    /// 
    /// </summary>
    [TestClass()]
    public class AnimalTests
    {
        /// <summary>
        /// Animals the test.
        /// </summary>
        [TestMethod()]
        public void AnimalTest()
        {
            Animal animal = new Animal();
            Assert.IsNotNull(animal);
        }

        /// <summary>
        /// Animals the test1.
        /// </summary>
        [TestMethod()]
        public void AnimalTest1()
        {
            Animal animal = new Animal("betty", TypeOfAnimal.bird, "crows", 1, 789456);
            Assert.IsNotNull(animal);
            Assert.IsTrue(animal.Name == "betty");
            Assert.IsTrue(animal.typeofanimal == TypeOfAnimal.bird);
            Assert.IsTrue(animal.subspecies == "crows");
            Assert.IsTrue(animal.Weight == 1);
            Assert.IsTrue(animal.Quantity == 789456);
        }

        /// <summary>
        /// Strings to type of animal test.
        /// </summary>
        [TestMethod()]
        public void StringToTypeOfAnimalTest()
        {
            Assert.IsTrue(Animal.StringToTypeOfAnimal("bird") == TypeOfAnimal.bird);
            Assert.IsTrue(Animal.StringToTypeOfAnimal("fish") == TypeOfAnimal.fish);
            Assert.IsTrue(Animal.StringToTypeOfAnimal("cat") == TypeOfAnimal.cat);
            Assert.IsTrue(Animal.StringToTypeOfAnimal("dog") == TypeOfAnimal.dog);
            Assert.IsTrue(Animal.StringToTypeOfAnimal("mammal") == TypeOfAnimal.mammal);
            Assert.IsTrue(Animal.StringToTypeOfAnimal("bunny") == TypeOfAnimal.bunny);
            Assert.IsTrue(Animal.StringToTypeOfAnimal("rabbit") == TypeOfAnimal.rabbit);
            Exception exception = null;
            try
            {
                Animal animal = new Animal();
                animal.typeofanimal = Animal.StringToTypeOfAnimal("rabbit");
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            //Assert.IsInstanceOfType(exception, typeof(Exception));
        }

        /// <summary>
        /// Converts to stringtest.
        /// </summary>
        [TestMethod()]
        public void ToStringTest()
        {
            Animal animal = new Animal("betty", TypeOfAnimal.bird, "crows", 1, 789456);
            Assert.IsTrue("betty bird crows 1 789456" == animal.ToString());
        }
    }
}