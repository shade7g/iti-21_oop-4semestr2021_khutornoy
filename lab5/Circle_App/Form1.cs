﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Circle_Library;

namespace Circle_App
{
    public partial class Form1 : Form
    {
        Bitmap bitmap;
        PictureBox pictureBox;
        bool isCircle = false;
        int moveSide = 1;

        /// <summary>
        /// Инициализатор формы Form1.
        /// </summary>
        public Form1()
        {
            InitializeComponent();

            ChangeColor_Panel.Color = Color.Black;

            ColorChange_Button.Click += ColorChange_Click;
            DrawCircle_Button.Click += DrawCircle_Click;
        }

        /// <summary>
        /// Открыть панель с изменением цвета.
        /// </summary>
        private void ColorChange_Click(object sender, EventArgs e)
        {
            ChangeColor_Panel.ShowDialog();
        }

        /// <summary>
        /// Валидация данных круга и его отрисовка.
        /// </summary>
        private void DrawCircle_Click(object sender, EventArgs e)
        {
            int frameSize = 10;
            int circleSize;

            if (int.TryParse(CircleSize_Box.Text, out circleSize))
            {
                if (isCircle)
                {
                    MainPanel.Controls.Remove(pictureBox);
                }
                else
                {
                    isCircle = true;
                }

                var circle = new Circle(circleSize, ChangeColor_Panel.Color);

                pictureBox = new PictureBox()
                {
                    Width = circle.Size + frameSize,
                    Height = circle.Size + frameSize,
                    BackColor = MainPanel.BackColor,
                };

                pictureBox.Location = new Point((MainPanel.Width - pictureBox.Width) / 2, (MainPanel.Height - pictureBox.Height) / 2);

                bitmap = new Bitmap(pictureBox.Width, pictureBox.Height);

                pictureBox.Image = DrawingCircle.Draw(circle, new Point((pictureBox.Width - circle.Size) / 2, (pictureBox.Height - circle.Size) / 2), bitmap);

                MainPanel.MouseMove += CircleMove;
                MainPanel.Controls.Add(pictureBox);
            }
        }

        /// <summary>
        /// Движение круга.
        /// </summary>
        private void CircleMove(object sender, EventArgs e)
        {
            int verticalM = 0;
            int horizontalM = 0;

            if (pictureBox.Location.X == (MainPanel.Width - pictureBox.Width) || pictureBox.Location.Y == (MainPanel.Height - pictureBox.Height))
            {
                moveSide = -1;
            }

            if (pictureBox.Location.X == 0 || pictureBox.Location.Y == 0)
            {
                moveSide = 1;
            }

            if(VertivalMove_Box.Checked && !HorizontalMove_Box.Checked)
            {
                verticalM = moveSide;
            }

            if (HorizontalMove_Box.Checked && !VertivalMove_Box.Checked)
            {
                horizontalM = moveSide;
            }

            pictureBox.Location = new Point(pictureBox.Location.X + horizontalM, pictureBox.Location.Y + verticalM);          
        }    
    }
}
