﻿namespace ClassLibrary
{
    /// <summary>
    /// FactoryShop
    /// </summary>
    /// <seealso cref="ClassLibrary.Factory" />
    public class FactoryShop : Factory
    {
        /// <summary>
        /// The connection string
        /// </summary>
        private string _connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="FactoryShop"/> class.
        /// </summary>
        /// <param name="_connectionString">The connection string.</param>
        public FactoryShop(string _connectionString)
        {
            this._connectionString = _connectionString;
        }

        /// <summary>
        /// Gets the product.
        /// </summary>
        /// <returns></returns>
        public override IAnimals GetAnimals()
        {
            return new AnimalsLinq(_connectionString);
        }

        /// <summary>
        /// Gets the producer.
        /// </summary>
        /// <returns></returns>
        public override ITypes GetTypes()
        {
            return new TypesLinq(_connectionString);
        }

        /// <summary>
        /// Gets the main shop.
        /// </summary>
        /// <returns></returns>
        public override ISubTypes GetSubTypes()
        {
            return new SubTypesLinq(_connectionString);
        }
    }
}