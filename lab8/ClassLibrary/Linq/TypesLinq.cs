﻿using ClassLibrary.Object;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

namespace ClassLibrary
{
    /// <summary>
    /// ProducerLinq
    /// </summary>
    /// <seealso cref="ClassLibrary.IProducer" />
    public class TypesLinq : ITypes
    {
        /// <summary>
        /// The data context
        /// </summary>
        private DataContext _dataContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProducerLinq"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public TypesLinq(string connectionString)
        {
            _dataContext = new DataContext(connectionString);
        }

        /// <summary>
        /// Gets the notes.
        /// </summary>
        /// <returns></returns>
        public Types[] GetNotes()
        {
            var objectList = from i
                             in _dataContext.GetTable<Types>()
                             select i;
            List<Types> elements = new List<Types>();
            foreach (Types element in objectList)
            {
                elements.Add(element);
            }
            return elements.ToArray();
        }

        /// <summary>
        /// Gets the note.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public Types GetNote(int id)
        {
            var objectList = from i
                             in _dataContext.GetTable<Types>()
                             where i.TypeId == id
                             select i;
            return objectList.First();
        }

        /// <summary>
        /// Gets the note.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        public Types GetNote(Types element) => GetNote(element.TypeId);

        /// <summary>
        /// Inserts the specified add element.
        /// </summary>
        /// <param name="addElement">The add element.</param>
        /// <returns></returns>
        public bool Insert(Types addElement)
        {
            _dataContext.GetTable<Types>().InsertOnSubmit(addElement);
            _dataContext.SubmitChanges();
            var elements = GetNotes();
            addElement.TypeId = elements[elements.Length - 1].TypeId;
            return true;
        }

        /// <summary>
        /// Updates the specified edit.
        /// </summary>
        /// <param name="edit">The edit.</param>
        /// <param name="newElement">The new element.</param>
        /// <returns></returns>
        public bool Update(Types edit, Types newElement)
        {
            _dataContext.GetTable<Types>().DeleteOnSubmit(edit);
            _dataContext.GetTable<Types>().InsertOnSubmit(newElement);
            _dataContext.SubmitChanges();
            var elements = GetNotes();
            newElement.TypeId = elements[elements.Length - 1].TypeId;
            return true;
        }

        /// <summary>
        /// Deletes the specified delete.
        /// </summary>
        /// <param name="delete">The delete.</param>
        /// <returns></returns>
        public bool Delete(Types delete)
        {
            _dataContext.GetTable<Types>().DeleteOnSubmit(delete);
            _dataContext.SubmitChanges();
            return true;
        }
    }
}