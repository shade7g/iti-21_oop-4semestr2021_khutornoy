﻿using ClassLibrary.Object;

namespace ClassLibrary
{
    /// <summary>
    /// ProducerLetter
    /// </summary>
    public class TypesLetter
    {
        /// <summary>
        /// Gets the name of the producer.
        /// </summary>
        /// <value>
        /// The name of the producer.
        /// </value>
        public string TypeName { get; private set; }
        /// <summary>
        /// Gets the identifier producer.
        /// </summary>
        /// <value>
        /// The identifier producer.
        /// </value>
        public int IDType { get; private set; }
        /// <summary>
        /// Gets the information.
        /// </summary>
        /// <value>
        /// The information.
        /// </value>
        public string Info { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProducerLetter"/> class.
        /// </summary>
        /// <param name="producer">The producer.</param>
        public TypesLetter(Types producer)
        {
            IDType = producer.TypeId;
            TypeName = producer.Name;

            Info = $"№{IDType}, вид: {TypeName}.";
        }
    }
}