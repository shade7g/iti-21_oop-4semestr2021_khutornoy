﻿using System;

namespace TestObjects_Library
{
    /// <summary>
    /// Класс для примера сериализации и рефлексии.
    /// </summary>
    [Serializable]
    public class Man
    {
        public string Name { get; set; }

        public int Age { get; set; }

        public int Weight { get; set; }

        public int Height { get; set; }

        public Man() { }

        public Man(string name, int age, int weight, int height)
        {
            if (String.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("Имя не было введено!");
            }

            if (age < 1 || age > 120)
            {
                throw new ArgumentException("Введён не корректный возраст!");
            }

            if (weight < 1 || weight > 300)
            {
                throw new ArgumentException("Введён не корректный вес человека!");
            }

            if (height < 1 || height > 250)
            {
                throw new ArgumentException("Введён не корректный рост человека!");
            }

            Name = name;
            Age = age;
            Weight = weight;
            Height = height;
        }

        public override string ToString()
        {
            return $"Name - {Name};\nAge - {Age};\nWeight - {Weight};\nHeight - {Height}. ";
        }
    }
}
