﻿using System;

namespace TestObjects_Library
{
    /// <summary>
    /// Лёгковой автомобиль.
    /// </summary>
    public class PassengerCar : Car
    {
        public PassengerCar(string mark, int mileage, int price, float maxSpeed, float overclocking) : base(mark, mileage, price, maxSpeed, overclocking) { }
    }
}
