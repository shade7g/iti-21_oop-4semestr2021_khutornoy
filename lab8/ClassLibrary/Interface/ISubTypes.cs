﻿using ClassLibrary.Object;

namespace ClassLibrary
{
    /// <summary>
    /// IProduct
    /// </summary>
    public interface ISubTypes
    {
        /// <summary>
        /// Gets the notes.
        /// </summary>
        /// <returns></returns>
        SubTypes[] GetNotes();

        /// <summary>
        /// Gets the note.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        SubTypes GetNote(int id);

        /// <summary>
        /// Gets the note.
        /// </summary>
        /// <param name="servicez">The servicez.</param>
        /// <returns></returns>
        SubTypes GetNote(SubTypes servicez);

        /// <summary>
        /// Inserts the specified add servicez.
        /// </summary>
        /// <param name="addServicez">The add servicez.</param>
        /// <returns></returns>
        bool Insert(SubTypes addServicez);

        /// <summary>
        /// Updates the specified edit servicez.
        /// </summary>
        /// <param name="editServicez">The edit servicez.</param>
        /// <param name="newServicez">The new servicez.</param>
        /// <returns></returns>
        bool Update(SubTypes editServicez, SubTypes newServicez);

        /// <summary>
        /// Deletes the specified delete servicez.
        /// </summary>
        /// <param name="deleteServicez">The delete servicez.</param>
        /// <returns></returns>
        bool Delete(SubTypes deleteServicez);
    }
}