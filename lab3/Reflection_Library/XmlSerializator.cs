﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Reflection_Library
{
    /// <summary>
    /// Сериализация объектов.
    /// </summary>
    public class XmlSerializator
    {
        private string path;

        /// <summary>
        /// Инициализатор класс XmlSerializator.
        /// </summary>
        /// <param name="path"> Путь к файлу. </param>
        /// <param name="objectName"> Название обекта. </param>
        public XmlSerializator(string path, string objectName)
        {           
            if (String.IsNullOrWhiteSpace(objectName))
            {
                throw new ArgumentException("Было переданно пустое название!");
            }

            path += objectName + ".xml";

            this.path = path;                   
        }

        /// <summary>
        /// Сериализация объекта.
        /// </summary>
        /// <param name="obj"> Объект для сериализации. </param>
        public void Serialize(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("Попытка сериализации пустого объекта!");
            }

            var formatter = new XmlSerializer(obj.GetType());

            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                formatter.Serialize(fs, obj);
            }
        }
    }
}
