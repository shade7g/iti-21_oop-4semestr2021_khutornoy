﻿using ClassLibrary.Object;

namespace ClassLibrary
{
    /// <summary>
    /// IProducer
    /// </summary>
    public interface ITypes
    {
        /// <summary>
        /// Gets the notes.
        /// </summary>
        /// <returns></returns>
        Types[] GetNotes();

        /// <summary>
        /// Gets the note.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Types GetNote(int id);

        /// <summary>
        /// Gets the note.
        /// </summary>
        /// <param name="servicez">The servicez.</param>
        /// <returns></returns>
        Types GetNote(Types servicez);

        /// <summary>
        /// Inserts the specified add servicez.
        /// </summary>
        /// <param name="addServicez">The add servicez.</param>
        /// <returns></returns>
        bool Insert(Types addServicez);

        /// <summary>
        /// Updates the specified edit servicez.
        /// </summary>
        /// <param name="editServicez">The edit servicez.</param>
        /// <param name="newServicez">The new servicez.</param>
        /// <returns></returns>
        bool Update(Types editServicez, Types newServicez);

        /// <summary>
        /// Deletes the specified delete servicez.
        /// </summary>
        /// <param name="deleteServicez">The delete servicez.</param>
        /// <returns></returns>
        bool Delete(Types deleteServicez);
    }
}