﻿using System.Configuration;

namespace ClassLibrary
{
    /// <summary>
    /// Connection
    /// </summary>
    public class Connection
    {
        /// <summary>
        /// The connection string
        /// </summary>
        public static string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
    }
}