﻿
namespace ReflectionAndSerializations_App
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ReflectionOutLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ReflectionTypeBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.AgeBox = new System.Windows.Forms.TextBox();
            this.WeightBox = new System.Windows.Forms.TextBox();
            this.HeightBox = new System.Windows.Forms.TextBox();
            this.NameBox = new System.Windows.Forms.TextBox();
            this.GetDeserialization_Button = new System.Windows.Forms.Button();
            this.GetSerialization_Button = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.Serialization_label = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.ReflectionTypeBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(30, 68);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(626, 551);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.BackColor = System.Drawing.Color.IndianRed;
            this.panel3.Controls.Add(this.ReflectionOutLabel);
            this.panel3.Location = new System.Drawing.Point(60, 160);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(546, 373);
            this.panel3.TabIndex = 4;
            // 
            // ReflectionOutLabel
            // 
            this.ReflectionOutLabel.AutoSize = true;
            this.ReflectionOutLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ReflectionOutLabel.ForeColor = System.Drawing.Color.White;
            this.ReflectionOutLabel.Location = new System.Drawing.Point(38, 22);
            this.ReflectionOutLabel.Name = "ReflectionOutLabel";
            this.ReflectionOutLabel.Size = new System.Drawing.Size(0, 24);
            this.ReflectionOutLabel.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(55, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Вывести все:";
            // 
            // ReflectionTypeBox
            // 
            this.ReflectionTypeBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ReflectionTypeBox.FormattingEnabled = true;
            this.ReflectionTypeBox.Location = new System.Drawing.Point(60, 97);
            this.ReflectionTypeBox.Name = "ReflectionTypeBox";
            this.ReflectionTypeBox.Size = new System.Drawing.Size(121, 33);
            this.ReflectionTypeBox.TabIndex = 1;
            this.ReflectionTypeBox.SelectedIndexChanged += new System.EventHandler(this.ReflectionTypeBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(254, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Рефлексия";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.AgeBox);
            this.panel2.Controls.Add(this.WeightBox);
            this.panel2.Controls.Add(this.HeightBox);
            this.panel2.Controls.Add(this.NameBox);
            this.panel2.Controls.Add(this.GetDeserialization_Button);
            this.panel2.Controls.Add(this.GetSerialization_Button);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(690, 68);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(740, 551);
            this.panel2.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(572, 77);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "Рост:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(410, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 17);
            this.label6.TabIndex = 11;
            this.label6.Text = "Вес:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(241, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Возраст:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(75, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Имя:";
            // 
            // AgeBox
            // 
            this.AgeBox.Location = new System.Drawing.Point(212, 97);
            this.AgeBox.Name = "AgeBox";
            this.AgeBox.Size = new System.Drawing.Size(100, 22);
            this.AgeBox.TabIndex = 8;
            // 
            // WeightBox
            // 
            this.WeightBox.Location = new System.Drawing.Point(382, 97);
            this.WeightBox.Name = "WeightBox";
            this.WeightBox.Size = new System.Drawing.Size(100, 22);
            this.WeightBox.TabIndex = 7;
            // 
            // HeightBox
            // 
            this.HeightBox.Location = new System.Drawing.Point(536, 97);
            this.HeightBox.Name = "HeightBox";
            this.HeightBox.Size = new System.Drawing.Size(100, 22);
            this.HeightBox.TabIndex = 6;
            // 
            // NameBox
            // 
            this.NameBox.Location = new System.Drawing.Point(51, 97);
            this.NameBox.Name = "NameBox";
            this.NameBox.Size = new System.Drawing.Size(100, 22);
            this.NameBox.TabIndex = 5;
            // 
            // GetDeserialization_Button
            // 
            this.GetDeserialization_Button.Location = new System.Drawing.Point(413, 173);
            this.GetDeserialization_Button.Name = "GetDeserialization_Button";
            this.GetDeserialization_Button.Size = new System.Drawing.Size(154, 33);
            this.GetDeserialization_Button.TabIndex = 4;
            this.GetDeserialization_Button.Text = "Десериализация";
            this.GetDeserialization_Button.UseVisualStyleBackColor = true;
            this.GetDeserialization_Button.Click += new System.EventHandler(this.GetDeserialization_Button_Click);
            // 
            // GetSerialization_Button
            // 
            this.GetSerialization_Button.Location = new System.Drawing.Point(120, 173);
            this.GetSerialization_Button.Name = "GetSerialization_Button";
            this.GetSerialization_Button.Size = new System.Drawing.Size(142, 33);
            this.GetSerialization_Button.TabIndex = 3;
            this.GetSerialization_Button.Text = "Сериализация";
            this.GetSerialization_Button.UseVisualStyleBackColor = true;
            this.GetSerialization_Button.Click += new System.EventHandler(this.GetSerialization_Button_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.IndianRed;
            this.panel4.Controls.Add(this.Serialization_label);
            this.panel4.Location = new System.Drawing.Point(99, 232);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(523, 301);
            this.panel4.TabIndex = 2;
            // 
            // Serialization_label
            // 
            this.Serialization_label.AutoSize = true;
            this.Serialization_label.Location = new System.Drawing.Point(43, 44);
            this.Serialization_label.Name = "Serialization_label";
            this.Serialization_label.Size = new System.Drawing.Size(0, 17);
            this.Serialization_label.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(94, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(561, 50);
            this.label2.TabIndex = 1;
            this.label2.Text = "Сериализация.\r\nДля примера используем объект описывающий человека.";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Brown;
            this.ClientSize = new System.Drawing.Size(1442, 672);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox ReflectionTypeBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label ReflectionOutLabel;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label Serialization_label;
        private System.Windows.Forms.Button GetSerialization_Button;
        private System.Windows.Forms.Button GetDeserialization_Button;
        private System.Windows.Forms.TextBox AgeBox;
        private System.Windows.Forms.TextBox WeightBox;
        private System.Windows.Forms.TextBox HeightBox;
        private System.Windows.Forms.TextBox NameBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
    }
}

