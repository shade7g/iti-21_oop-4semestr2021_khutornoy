﻿using System;
using System.Drawing;

namespace Circle_Library
{
    /// <summary>
    /// Круг.
    /// </summary>
    public class Circle
    {
        private int size;
        private Color color;

        /// <summary>
        /// Инициализатор класса Circle.
        /// </summary>
        /// <param name="size"> Размер. </param>
        /// <param name="color"> Цвет. </param>
        public Circle(int size, Color color)
        {
            if(size <= 0)
            {
                throw new ArgumentException("Размер круга должен быть больше > 0!");
            }

            if (color == null)
            {
                throw new ArgumentNullException("Цвет прямоугольника не указан !");
            }

            this.size = size;
            this.color = color;
        }

        /// <summary>
        /// Размер круга.
        /// </summary>
        public int Size
        {
            get { return size; }        
        }

        /// <summary>
        /// Цвет.
        /// </summary>
        public Color Color
        {
            get { return color; }
        }
    }
}
