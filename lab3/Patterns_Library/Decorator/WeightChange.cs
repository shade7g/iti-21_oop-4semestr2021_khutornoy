﻿using System;
using TestObjects_Library;

namespace Patterns_Library
{
    /// <summary>
    /// Декорирование веса человека.
    /// </summary>
    public class WeightChange : Decorator
    {
        public WeightChange(Man man, int additionalWeight) : base(man)
        {
            if (additionalWeight <= 0)
            {
                throw new ArgumentException("Вы передаёте неподходящее значение!", nameof(additionalWeight));
            }

            Weight += additionalWeight;
        }
    }
}
