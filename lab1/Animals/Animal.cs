﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    /// <summary>
    /// 
    /// </summary>
    public class Animal
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Animal" /> class.
        /// </summary>
        public Animal()
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Animal" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="typeofanimal">The typeofanimal.</param>
        /// <param name="subspecies">The subspecies.</param>
        /// <param name="weight">The weight.</param>
        /// <param name="quantity">The quantity.</param>
        public Animal(string name, TypeOfAnimal typeofanimal, /*Subspecies*/ string subspecies, float weight, int quantity)
        {
            Name = name;
            this.typeofanimal = typeofanimal;
            this.subspecies = subspecies;
            Weight = weight;
            Quantity = quantity;
        }
        /// <summary>
        /// Strings to type of animal.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns></returns>
        public static TypeOfAnimal StringToTypeOfAnimal(string str)
        {

            TypeOfAnimal typeofanimal;
            switch (str)
            {
                case "bird": typeofanimal = TypeOfAnimal.bird; break;
                case "fish": typeofanimal = TypeOfAnimal.fish; break;
                case "cat": typeofanimal = TypeOfAnimal.cat; break;
                case "dog": typeofanimal = TypeOfAnimal.dog; break;
                case "mammal": typeofanimal = TypeOfAnimal.mammal; break;
                case "bunny": typeofanimal = TypeOfAnimal.bunny; break;
                case "rabbit": typeofanimal = TypeOfAnimal.rabbit; break;
                default: throw new Exception("Such animal type doesn't exist here!");
                //default: typeofanimal = TypeOfAnimal.bird; break;
            }
            return typeofanimal;
        }
        /*public static Subspecies StringToSubspecies(string str)
        {

            Subspecies subspecies;
            switch (str)
            {
                case "crows": subspecies = Subspecies.crows; break;
                case "perches": subspecies = Subspecies.perches; break;
                case "dolphin": subspecies = Subspecies.dolphin; break;
                //default: throw new Exception("Such animal subspecies doesn't exist here!");
                default: subspecies = Subspecies.crows; break;
            }
            return subspecies;
        }*/
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the typeofanimal.
        /// </summary>
        /// <value>
        /// The typeofanimal.
        /// </value>
        public TypeOfAnimal typeofanimal { get; set; }
        /// <summary>
        /// Gets or sets the subspecies.
        /// </summary>
        /// <value>
        /// The subspecies.
        /// </value>
        public /*Subspecies*/ string subspecies { get; set; }
        /// <summary>
        /// Gets or sets the weight.
        /// </summary>
        /// <value>
        /// The weight.
        /// </value>
        public float Weight { get; set; }
        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        /// <value>
        /// The quantity.
        /// </value>
        public int Quantity { get; set; }
        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return $"{Name} {typeofanimal} {subspecies} {Weight} {Quantity}";
        }
    }
}