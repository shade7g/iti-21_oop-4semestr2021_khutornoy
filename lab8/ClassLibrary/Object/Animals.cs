﻿using System;
using System.Data.Linq.Mapping;

namespace ClassLibrary.Object
{
    /// <summary>
    /// MainShop
    /// </summary>
    [Table(Name = "Animals")]
    public class Animals
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [Column(Name = "animalid", IsPrimaryKey = true, IsDbGenerated = true, DbType = "int")]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        [Column(Name = "name", DbType = "varchar(MAX)")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        [Column(Name = "typeid", DbType = "int")]
        public int TypeID { get; set; }

        /// <summary>
        /// Gets or sets the product identifier.
        /// </summary>
        /// <value>
        /// The product identifier.
        /// </value>
        [Column(Name = "subtypeid", DbType = "int")]
        public int SubTypeID { get; set; }

        /// <summary>
        /// Gets or sets the count.
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        [Column(Name = "weight", DbType = "int")]
        public int Weight { get; set; }

        /// <summary>
        /// Gets or sets the cost.
        /// </summary>
        /// <value>
        /// The cost.
        /// </value>
        [Column(Name = "quantity", DbType = "int")]
        public int Quantity { get; set; }
    }
}
