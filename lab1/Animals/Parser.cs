﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace Animals
{
    /// <summary>
    /// 
    /// </summary>
    public static class Parser
    {
        /// <summary>
        /// Adds the specified path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="animal">The animal.</param>
        /// <param name="save">The save.</param>
        public static void Add(string path, Animal animal, string save)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(path);
            CreateRoot(xDoc, animal);
            xDoc.Save(save);
        }
        /// <summary>
        /// Deletes the specified path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="animals">The animals.</param>
        public static void Delete(string path, Animal[] animals)
        {

            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(path);
            XmlElement xRoot;
            XmlNode firstNode;
            for (int i = 0; i < animals.Length + 1; i++)
            {
                xRoot = xDoc.DocumentElement;
                firstNode = xRoot.LastChild;
                xRoot.RemoveChild(firstNode);
            }
            for (int i = 0; i < animals.Length; i++)
                CreateRoot(xDoc, animals[i]);
            xDoc.Save(path);
        }
        /// <summary>
        /// Edits the specified path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="animals">The animals.</param>
        /// <param name="save">The save.</param>
        public static void Edit(string path, Animal[] animals, string save)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(path);
            XmlElement xRoot;
            XmlNode firstNode;
            for (int i = 0; i < animals.Length; i++)
            {
                xRoot = xDoc.DocumentElement;
                firstNode = xRoot.LastChild;
                xRoot.RemoveChild(firstNode);
            }
            for (int i = 0; i < animals.Length; i++)
                CreateRoot(xDoc, animals[i]);
            xDoc.Save(save);
        }
        /// <summary>
        /// Creates the root.
        /// </summary>
        /// <param name="xDoc">The x document.</param>
        /// <param name="animal">The animal.</param>
        private static void CreateRoot(XmlDocument xDoc, Animal animal)
        {
            XmlElement xRoot = xDoc.DocumentElement;
            XmlElement animalElem = xDoc.CreateElement("animal");
            XmlElement nameElem = xDoc.CreateElement("name");
            XmlElement typeOfAnimalElem = xDoc.CreateElement("type");
            XmlElement subspeciesElem = xDoc.CreateElement("subspecies");
            XmlElement weightElem = xDoc.CreateElement("weight");
            XmlElement quantityElem = xDoc.CreateElement("quantity");
            XmlText nameText = xDoc.CreateTextNode(animal.Name.ToString());
            XmlText typeOfAnimalText = xDoc.CreateTextNode(animal.typeofanimal.ToString());
            XmlText subspeciesText = xDoc.CreateTextNode(animal.subspecies.ToString());
            XmlText weightText = xDoc.CreateTextNode(animal.Weight.ToString());
            XmlText quantityText = xDoc.CreateTextNode(animal.Quantity.ToString());

            nameElem.AppendChild(nameText);
            typeOfAnimalElem.AppendChild(typeOfAnimalText);
            subspeciesElem.AppendChild(subspeciesText);
            weightElem.AppendChild(weightText);
            quantityElem.AppendChild(quantityText);

            animalElem.AppendChild(nameElem);
            animalElem.AppendChild(typeOfAnimalElem);
            animalElem.AppendChild(subspeciesElem);
            animalElem.AppendChild(weightElem);
            animalElem.AppendChild(quantityElem);

            xRoot.AppendChild(animalElem);
        }
        /// <summary>
        /// Planets the parser.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public static Planet PlanetParser(string path)
        {
            Planet planet = new Planet("animal");
            Animal animal = new Animal();
            using (XmlTextReader reader = new XmlTextReader(path))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement("animal") && !reader.IsEmptyElement)
                    {
                        while (reader.Read())
                        {
                            if (reader.IsStartElement("name"))
                                animal.Name = reader.ReadString();
                            else if (reader.IsStartElement("type"))
                                animal.typeofanimal = Animal.StringToTypeOfAnimal(reader.ReadString());
                                //animal.typeofanimal = reader.ReadString();
                            else if (reader.IsStartElement("subspecies"))
                                //animal.subspecies = Animal.StringToSubspecies(reader.ReadString());
                                animal.subspecies = reader.ReadString();
                            else if (reader.IsStartElement("weight"))
                                animal.Weight = float.Parse(reader.ReadString());
                            else if (reader.IsStartElement("quantity"))
                            {
                                animal.Quantity = int.Parse(reader.ReadString());
                                break;
                            }
                        }
                        planet.Animals.Add(animal);
                        animal = new Animal();
                    }
                }
            }
            return planet;
        }
    }
}