﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Reflection_Library
{
    /// <summary>
    /// Информация об элементах класса (Рефлексия).
    /// </summary>
    public class InfoAboutClass
    {
        private Type myType;

        /// <summary>
        /// Инициализатор класса InfoAboutClass.
        /// </summary>
        /// <param name="myType"> Тип класса. </param>
        public InfoAboutClass(Type myType)
        {
            if (myType == null)
            {
                throw new ArgumentNullException("Был передаёте пустой тип!");
            }

            this.myType = myType;
        }

        /// <summary>
        /// Получение методов класса.
        /// </summary>
        public List<string> GetMethods()
        {
            var methods = new List<string>();

            foreach (MethodInfo method in myType.GetMethods())
            {
                var allMethod = String.Empty;
                var modificator = String.Empty;

                if (method.IsStatic)
                {
                    modificator += "static ";
                }
                    
                if (method.IsVirtual)
                {
                    modificator += "virtual";
                }

                allMethod = $"{modificator} {method.ReturnType.Name} {method.Name} (";

                ParameterInfo[] parameters = method.GetParameters();

                for (int i = 0; i < parameters.Length; i++)
                {
                    allMethod += $"{parameters[i].ParameterType.Name} {parameters[i].Name}";

                    if (i + 1 < parameters.Length)
                    {
                        allMethod += ", ";
                    }
                        
                }

                allMethod += ")";

                methods.Add(allMethod);
            }

            return methods;
        }

        /// <summary>
        /// Получение свойств класса.
        /// </summary>
        public List<string> GetProperty()
        {
            var property = new List<string>();

            foreach (PropertyInfo prop in myType.GetProperties())
            {
                property.Add($"{prop.PropertyType} {prop.Name}");
            }

            return property;
        }

        /// <summary>
        /// Получение полей класса.
        /// </summary>
        public List<string> GetFields()
        {
            var fields = new List<string>();

            foreach (FieldInfo field in myType.GetFields())
            {
                fields.Add($"{field.FieldType} {field.Name}");
            }

            return fields;
        }

        /// <summary>
        /// Нахождение определённых элементов класса.
        /// </summary>
        /// <param name="memberTypes"> Тип элементов. </param>
        /// <returns> Лист с элементами указанного типа. </returns>
        private List<string> GetMembers(MemberTypes memberTypes)
        {
            var members = new List<string>();

            foreach (MemberInfo mi in myType.GetMembers())
            {
                if (mi.MemberType == memberTypes)
                {
                    members.Add($"{mi.MemberType} {mi.Name}");
                }
            }

            return members;
        }
    }
}
