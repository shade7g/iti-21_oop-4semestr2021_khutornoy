﻿namespace WindowsFormsApp1
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.addKind = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.addAnimalName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.addPopulationSize = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.addSubSpecies = new System.Windows.Forms.ComboBox();
            this.addWeight = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.add = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.remove = new System.Windows.Forms.Button();
            this.names = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.upWeight = new System.Windows.Forms.NumericUpDown();
            this.upSubSpecies = new System.Windows.Forms.ComboBox();
            this.upPopulationSize = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.upKind = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.upAnimalName = new System.Windows.Forms.ComboBox();
            this.AnimalName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kind = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Subspecies = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PopulationSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addPopulationSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upPopulationSize)).BeginInit();
            this.SuspendLayout();
            // 
            // addKind
            // 
            this.addKind.FormattingEnabled = true;
            this.addKind.Location = new System.Drawing.Point(31, 124);
            this.addKind.Name = "addKind";
            this.addKind.Size = new System.Drawing.Size(121, 24);
            this.addKind.TabIndex = 0;
            this.addKind.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AnimalName,
            this.Kind,
            this.Subspecies,
            this.Weight,
            this.PopulationSize});
            this.dataGridView1.Location = new System.Drawing.Point(385, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(833, 593);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // addAnimalName
            // 
            this.addAnimalName.Location = new System.Drawing.Point(31, 77);
            this.addAnimalName.Name = "addAnimalName";
            this.addAnimalName.Size = new System.Drawing.Size(121, 22);
            this.addAnimalName.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Название";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Вид";
            // 
            // addPopulationSize
            // 
            this.addPopulationSize.Location = new System.Drawing.Point(31, 279);
            this.addPopulationSize.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.addPopulationSize.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.addPopulationSize.Name = "addPopulationSize";
            this.addPopulationSize.Size = new System.Drawing.Size(121, 22);
            this.addPopulationSize.TabIndex = 6;
            this.addPopulationSize.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Подвид";
            // 
            // addSubSpecies
            // 
            this.addSubSpecies.FormattingEnabled = true;
            this.addSubSpecies.Location = new System.Drawing.Point(31, 171);
            this.addSubSpecies.Name = "addSubSpecies";
            this.addSubSpecies.Size = new System.Drawing.Size(121, 24);
            this.addSubSpecies.TabIndex = 8;
            // 
            // addWeight
            // 
            this.addWeight.Location = new System.Drawing.Point(31, 222);
            this.addWeight.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.addWeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.addWeight.Name = "addWeight";
            this.addWeight.Size = new System.Drawing.Size(121, 22);
            this.addWeight.TabIndex = 9;
            this.addWeight.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 202);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Средний вес";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 257);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(132, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Размер популяции";
            // 
            // add
            // 
            this.add.Location = new System.Drawing.Point(43, 324);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(87, 23);
            this.add.TabIndex = 12;
            this.add.Text = "Добавить";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 426);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "Название";
            // 
            // remove
            // 
            this.remove.Location = new System.Drawing.Point(43, 498);
            this.remove.Name = "remove";
            this.remove.Size = new System.Drawing.Size(90, 23);
            this.remove.TabIndex = 15;
            this.remove.Text = "Удалить";
            this.remove.UseVisualStyleBackColor = true;
            this.remove.Click += new System.EventHandler(this.remove_Click);
            // 
            // names
            // 
            this.names.FormattingEnabled = true;
            this.names.Location = new System.Drawing.Point(31, 456);
            this.names.Name = "names";
            this.names.Size = new System.Drawing.Size(121, 24);
            this.names.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(39, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 25);
            this.label7.TabIndex = 17;
            this.label7.Text = "Добавить";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(39, 384);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 25);
            this.label8.TabIndex = 18;
            this.label8.Text = "Удалить";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(212, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 25);
            this.label9.TabIndex = 19;
            this.label9.Text = "Обновить";
            // 
            // upWeight
            // 
            this.upWeight.Location = new System.Drawing.Point(217, 222);
            this.upWeight.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.upWeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.upWeight.Name = "upWeight";
            this.upWeight.Size = new System.Drawing.Size(121, 22);
            this.upWeight.TabIndex = 27;
            this.upWeight.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // upSubSpecies
            // 
            this.upSubSpecies.FormattingEnabled = true;
            this.upSubSpecies.Location = new System.Drawing.Point(217, 171);
            this.upSubSpecies.Name = "upSubSpecies";
            this.upSubSpecies.Size = new System.Drawing.Size(121, 24);
            this.upSubSpecies.TabIndex = 26;
            // 
            // upPopulationSize
            // 
            this.upPopulationSize.Location = new System.Drawing.Point(217, 277);
            this.upPopulationSize.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.upPopulationSize.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.upPopulationSize.Name = "upPopulationSize";
            this.upPopulationSize.Size = new System.Drawing.Size(121, 22);
            this.upPopulationSize.TabIndex = 24;
            this.upPopulationSize.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(214, 57);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 17);
            this.label14.TabIndex = 22;
            this.label14.Text = "Название";
            // 
            // upKind
            // 
            this.upKind.FormattingEnabled = true;
            this.upKind.Location = new System.Drawing.Point(217, 122);
            this.upKind.Name = "upKind";
            this.upKind.Size = new System.Drawing.Size(121, 24);
            this.upKind.TabIndex = 20;
            this.upKind.SelectedIndexChanged += new System.EventHandler(this.upKind_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(228, 324);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 23);
            this.button1.TabIndex = 30;
            this.button1.Text = "Обновить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // upAnimalName
            // 
            this.upAnimalName.FormattingEnabled = true;
            this.upAnimalName.Location = new System.Drawing.Point(217, 74);
            this.upAnimalName.Name = "upAnimalName";
            this.upAnimalName.Size = new System.Drawing.Size(121, 24);
            this.upAnimalName.TabIndex = 31;
            // 
            // AnimalName
            // 
            this.AnimalName.HeaderText = "AnimalName";
            this.AnimalName.MinimumWidth = 6;
            this.AnimalName.Name = "AnimalName";
            this.AnimalName.Width = 125;
            // 
            // Kind
            // 
            this.Kind.HeaderText = "Kind";
            this.Kind.MinimumWidth = 6;
            this.Kind.Name = "Kind";
            this.Kind.Width = 125;
            // 
            // Subspecies
            // 
            this.Subspecies.HeaderText = "Subspecies";
            this.Subspecies.MinimumWidth = 6;
            this.Subspecies.Name = "Subspecies";
            this.Subspecies.Width = 125;
            // 
            // Weight
            // 
            this.Weight.HeaderText = "Weight";
            this.Weight.MinimumWidth = 6;
            this.Weight.Name = "Weight";
            this.Weight.Width = 125;
            // 
            // PopulationSize
            // 
            this.PopulationSize.HeaderText = "PopulationSize";
            this.PopulationSize.MinimumWidth = 6;
            this.PopulationSize.Name = "PopulationSize";
            this.PopulationSize.Width = 125;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(217, 101);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(33, 17);
            this.label15.TabIndex = 32;
            this.label15.Text = "Вид";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(217, 149);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 17);
            this.label13.TabIndex = 33;
            this.label13.Text = "Подвид";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(217, 202);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 17);
            this.label10.TabIndex = 34;
            this.label10.Text = "Средний вес";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(217, 257);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(132, 17);
            this.label11.TabIndex = 35;
            this.label11.Text = "Размер популяции";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1253, 617);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.upAnimalName);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.upWeight);
            this.Controls.Add(this.upSubSpecies);
            this.Controls.Add(this.upPopulationSize);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.upKind);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.names);
            this.Controls.Add(this.remove);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.add);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.addWeight);
            this.Controls.Add(this.addSubSpecies);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.addPopulationSize);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.addAnimalName);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.addKind);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addPopulationSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upPopulationSize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox addKind;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox addAnimalName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown addPopulationSize;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox addSubSpecies;
        private System.Windows.Forms.NumericUpDown addWeight;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button remove;
        private System.Windows.Forms.ComboBox names;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown upWeight;
        private System.Windows.Forms.ComboBox upSubSpecies;
        private System.Windows.Forms.NumericUpDown upPopulationSize;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox upKind;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox upAnimalName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AnimalName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kind;
        private System.Windows.Forms.DataGridViewTextBoxColumn Subspecies;
        private System.Windows.Forms.DataGridViewTextBoxColumn Weight;
        private System.Windows.Forms.DataGridViewTextBoxColumn PopulationSize;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
    }
}

