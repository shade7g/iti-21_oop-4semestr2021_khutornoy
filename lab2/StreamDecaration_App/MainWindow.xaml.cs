﻿using System;
using System.IO;
using System.Text;
using System.Windows;
using StreamDecoration_Library;
using System.Text.RegularExpressions;

namespace StreamDecaration_App
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string path = @"C:\Users\Max\source\repos\iti-21_oop-4semestr2021_khutornoy\lab2\testStream.txt";

        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Вывод текста.
        /// </summary>
        /// <returns> Время работы с потоком. </returns>
        private double OutputText()
        {
            ReadingStream rs = null;

            try
            {
                var textOut = new StringBuilder();
                rs = new ReadingStream(new FileStream(path, FileMode.Open));

                var text = rs.GetText();
                var sentences = Regex.Split(text, @"(?<=[!?.])\s+");

                textOut.Append("Текст: \n\n");

                foreach (string str in sentences)
                {
                    textOut.Append(str + "\n");
                }

                Text_Label.Content = textOut;

                return rs.RunTime;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                this.Close();
            }

            return 0;
        }

        /// <summary>
        /// Очистка текста и время работы с потоком.
        /// </summary>
        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Text_Label.Content = string.Empty;
            RunTime_Label.Content = string.Empty;
        }

        /// <summary>
        /// Вывод текста при нажатии на кнопку "OutputTextButton".
        /// </summary>
        private void OutputTextButton_Click(object sender, RoutedEventArgs e)
        {
            var runTime = OutputText();

            RunTime_Label.Content = $" Время работы с потоком = {runTime} мс.";
        }
    }
}
