﻿using Animals;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace AnimalShowcase
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Windows.Window" />
    /// <seealso cref="System.Windows.Markup.IComponentConnector" />
    public partial class MainWindow : Window
    {
        /// <summary>
        /// The planet
        /// </summary>
        private Planet planet;
        /// <summary>
        /// The path XSD
        /// </summary>
        private readonly string _pathXSD = @"C:\Users\Max\source\repos\iti-21_oop-4semestr2021_khutornoy\lab1\AnimalShowcase\bin\Debug\planet.xsd";
        /// <summary>
        /// The path XML
        /// </summary>
        private readonly string _pathXML = @"C:\Users\Max\source\repos\iti-21_oop-4semestr2021_khutornoy\lab1\AnimalShowcase\bin\Debug\planet.xml";
        /// <summary>
        /// The save
        /// </summary>
        private readonly string _save = @"C:\Users\Max\source\repos\iti-21_oop-4semestr2021_khutornoy\lab1\AnimalShowcase\bin\Debug\planet.xml";
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                planet = Parser.PlanetParser(_pathXML);
                InitView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Initializes the view.
        /// </summary>
        private void InitView()
        {
            try
            {
                CheckXSD(_pathXSD, _pathXML);
                data.Items.Clear();
                //XSD.Items.Clear();
                XML.Items.Clear();
                planet = Parser.PlanetParser(_pathXML);
                foreach (var da in planet.Animals)
                    data.Items.Add(da.ToString());
                using (StreamReader sr = new StreamReader(_pathXSD))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        //XSD.Items.Add(line);
                    }
                }
                using (StreamReader sr = new StreamReader(_pathXML))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        XML.Items.Add(line);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the buttonAdd control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Parser.Add(_pathXML, new Animal { Name = NameTextBox.Text, typeofanimal = Animal.StringToTypeOfAnimal(TypeAnTextBox.Text), subspecies = Animal.StringToSubspecies(SubspeciesTextBox.Text), Weight = float.Parse(WeightTextBox.Text), Quantity = int.Parse(QuantityTextBox.Text)}, _save);
                Parser.Add(_pathXML, new Animal { Name = NameTextBox.Text, typeofanimal = Animal.StringToTypeOfAnimal(typeOfAnimal.Text), subspecies = SubspeciesTextBox.Text, Weight = float.Parse(WeightTextBox.Text), Quantity = int.Parse(QuantityTextBox.Text) }, _save);
                if (CheckXSDBool(_pathXSD, _save))
                {
                    //Parser.Add(_pathXML, new Animal { Name = NameTextBox.Text, typeofanimal = Animal.StringToTypeOfAnimal(TypeAnTextBox.Text), subspecies = Animal.StringToSubspecies(SubspeciesTextBox.Text), Weight = float.Parse(WeightTextBox.Text), Quantity = int.Parse(QuantityTextBox.Text) }, _save);
                    Parser.Add(_pathXML, new Animal { Name = NameTextBox.Text, typeofanimal = Animal.StringToTypeOfAnimal(typeOfAnimal.Text), subspecies = SubspeciesTextBox.Text, Weight = float.Parse(WeightTextBox.Text), Quantity = int.Parse(QuantityTextBox.Text) }, _save);
                }
                else
                {
                    CheckXSD(_pathXSD, _save);
                }
                InitView();
            }
            catch { MessageBox.Show("-You have to put all the information into the brackets first\n-Wrong type"); }
        }

        /// <summary>
        /// Handles the Click event of the buttonEdit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void buttonEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                planet.Animals.RemoveAt(data.SelectedIndex);
                //planet.Animals.Insert(data.SelectedIndex, new Animal { Name = NameTextBox.Text, typeofanimal = Animal.StringToTypeOfAnimal(TypeAnTextBox.Text), subspecies = Animal.StringToSubspecies(SubspeciesTextBox.Text), Weight = float.Parse(WeightTextBox.Text), Quantity = int.Parse(QuantityTextBox.Text) });
                planet.Animals.Insert(data.SelectedIndex, new Animal { Name = NameTextBox.Text, typeofanimal = Animal.StringToTypeOfAnimal(typeOfAnimal.Text), subspecies = SubspeciesTextBox.Text, Weight = float.Parse(WeightTextBox.Text), Quantity = int.Parse(QuantityTextBox.Text) });

                Parser.Edit(_pathXML, planet.Animals.ToArray(), _save);
                if (CheckXSDBool(_pathXSD, _save))
                {
                    Parser.Edit(_pathXML, planet.Animals.ToArray(), _pathXML);
                }
                else
                {
                    CheckXSD(_pathXSD, _save);
                }
                InitView();
            }
            catch { MessageBox.Show("You have to select an item first"); }
        }

        /// <summary>
        /// Handles the Click event of the DeleteButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        /// <exception cref="Exception">Here's nothing to delete</exception>
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (data.SelectedValue == null)
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load(_pathXML);
                    XmlElement xRoot = xDoc.DocumentElement;
                    XmlNode firstNode = xRoot.LastChild;
                    if (xRoot.LastChild == null) throw new Exception("Here's nothing to delete");
                    xRoot.RemoveChild(firstNode);
                    xDoc.Save(_pathXML);

                }
                else
                {
                    planet.Animals.RemoveAt(data.SelectedIndex);
                    Parser.Delete(_pathXML, planet.Animals.ToArray());
                }
                InitView();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        /// <summary>
        /// Handles the Click event of the Find control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Find_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FindText();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Finds the text.
        /// </summary>
        /// <exception cref="Exception">Select data first</exception>
        private void FindText()
        {
            try
            {
                if (data.SelectedItem == null) throw new Exception("Select data first");
                string[] text = data.SelectedItem.ToString().Split();
                NameTextBox.Text = text[0];
                typeOfAnimal.Text= text[1];
                SubspeciesTextBox.Text = text[2];
                WeightTextBox.Text = text[3];
                QuantityTextBox.Text = text[4];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Checks the XSD.
        /// </summary>
        /// <param name="pathXSD">The path XSD.</param>
        /// <param name="pathXML">The path XML.</param>
        private void CheckXSD(string pathXSD, string pathXML)
        {
            XmlSchemaSet schemas = new XmlSchemaSet();
            schemas.Add(null, pathXSD);

            XDocument document = XDocument.Load(pathXML);

            document.Validate(schemas, (sender, validationEventArgs) =>
            {
                //MessageBox.Show(validationEventArgs.Message);
            });
        }

        /// <summary>
        /// Checks the XSD bool.
        /// </summary>
        /// <param name="pathXSD">The path XSD.</param>
        /// <param name="pathXML">The path XML.</param>
        /// <returns></returns>
        private bool CheckXSDBool(string pathXSD, string pathXML)
        {
            XmlSchemaSet schemas = new XmlSchemaSet();
            schemas.Add(null, pathXSD);

            XDocument document = XDocument.Load(pathXML);

            bool res = true;
            document.Validate(schemas, (sender, validationEventArgs) =>
            {
                res = false;
            });

            return res;
        }

        /// <summary>
        /// Handles the Click event of the Update control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Update_Click(object sender, RoutedEventArgs e)
        {
            InitView();
        }

        /// <summary>
        /// Handles the SelectionChanged event of the XML control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void XML_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        /*private void XSD_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }*/

        /// <summary>
        /// Handles the SelectionChanged event of the data control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void data_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        /// <summary>
        /// Handles the TextChanged event of the NameTextBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TextChangedEventArgs"/> instance containing the event data.</param>
        private void NameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        /// <summary>
        /// Handles the TextChanged event of the TypeAnTextBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TextChangedEventArgs"/> instance containing the event data.</param>
        private void TypeAnTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        /// <summary>
        /// Handles the TextChanged event of the SubspeciesTextBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TextChangedEventArgs"/> instance containing the event data.</param>
        private void SubspeciesTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        /// <summary>
        /// Handles the TextChanged event of the WeightTextBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TextChangedEventArgs"/> instance containing the event data.</param>
        private void WeightTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        /// <summary>
        /// Handles the TextChanged event of the QuantityTextBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TextChangedEventArgs"/> instance containing the event data.</param>
        private void QuantityTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void typeOfAnimal_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }
    }
}
