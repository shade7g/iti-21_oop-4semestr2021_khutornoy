﻿using System;
using System.Data.Linq.Mapping;

namespace ClassLibrary.Object
{

    /// <summary>
    /// Producer
    /// </summary>
    [Table(Name = "Subtypes")]
    public class SubTypes
    {
        /// <summary>
        /// Gets or sets the producer identifier.
        /// </summary>
        /// <value>
        /// The producer identifier.
        /// </value>
        [Column(Name = "subtypeId", IsPrimaryKey = true, IsDbGenerated = true, DbType = "int")]
        public int SubTypeId { get; set; }

        /// <summary>
        /// Gets or sets the name of the producer.
        /// </summary>
        /// <value>
        /// The name of the producer.
        /// </value>
        [Column(Name = "name", DbType = "varchar(MAX)")]
        public string Name { get; set; }
    }
}
