﻿using System.Collections.Generic;
using System.Text;

namespace Reflection_Library
{
    /// <summary>
    /// Обработка строк.
    /// </summary>
    public static class TextProcessing
    {
        /// <summary>
        /// Преобразование строк в текст.
        /// </summary>
        /// <param name="sentences"> Лист со строками. </param>
        /// <returns> Текст. </returns>
        public static StringBuilder GetSortedText(List<string> list)
        {
            var outText = new StringBuilder();

            foreach (string str in list)
            {
                outText.Append(str + "\n");
            }

            return outText;
        }
    }
}
