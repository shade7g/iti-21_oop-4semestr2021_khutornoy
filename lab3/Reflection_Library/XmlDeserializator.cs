﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Reflection_Library
{
    /// <summary>
    /// Десериализация объектов.
    /// </summary>
    public class XmlDeserializator
    {       
        private string path;

        /// <summary>
        /// Инициализатор класс XmlDeserializator.
        /// </summary>
        /// <param name="path"> Путь к файлу. </param>
        /// <param name="objectName"> Название обекта. </param>
        public XmlDeserializator(string path, string objectName)
        {
            path += objectName + ".xml";

            if (!File.Exists(path))
            {
                throw new FileNotFoundException($"Файл {path} не был найден!");
            }

            if (String.IsNullOrWhiteSpace(objectName))
            {
                throw new ArgumentException("Было переданно пустое название!");
            }

            this.path = path;
        }   

        /// <summary>
        /// Десериализации объекта.
        /// </summary>
        /// <param name="obj"> Объект для сравнения типа. </param>
        /// <returns> Считаный объект указонного типа. </returns>
        public object Deserialize(object obj)
        {
            var formatter = new XmlSerializer(obj.GetType());

            object newObj = null;

            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                newObj = formatter.Deserialize(fs);
            }

            return newObj;
        }
    }
}
