﻿using System;

namespace Patterns_Library
{
    /// <summary>
    /// Реализация конкретных классов .
    /// </summary>
    public abstract class NewManCreator
    {
        public abstract ManFactory CreateNewMan();
    }
}
