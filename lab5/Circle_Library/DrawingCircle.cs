﻿using System.Drawing;

namespace Circle_Library
{
    /// <summary>
    /// Отрисовка круга.
    /// </summary>
    public class DrawingCircle
    {
        /// <summary>
        /// Отрисовка круга.
        /// </summary>
        /// <param name="circle"> Круг. </param>
        /// <param name="center"> Координаты центра. </param>
        /// <param name="bitmap"> Рисунок. </param>
        /// <returns> Рисунок с отрисованными кругом. </returns>
        public static Bitmap Draw(Circle circle, Point center, Bitmap bitmap)
        {
            var graphics = Graphics.FromImage(bitmap);
            graphics.FillEllipse(new SolidBrush(circle.Color), new Rectangle(center.X, center.Y, circle.Size, circle.Size));

            return bitmap;
        }
    }
}
