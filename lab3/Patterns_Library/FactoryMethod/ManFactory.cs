﻿using TestObjects_Library;

namespace Patterns_Library
{
    /// <summary>
    /// Определяет интерфейс классов типа Man, объекты которого будут создаваться.
    /// </summary>
    public abstract class ManFactory
    {
        /// <summary>
        /// Получение нового объекта типа Man.
        /// </summary>
        /// <param name="man"></param>
        /// <returns></returns>
        public abstract Man Activation(Man man);
    }
}
