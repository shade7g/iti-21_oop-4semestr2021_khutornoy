﻿using System;

namespace TestObjects_Library
{
    /// <summary>
    /// Автомобиль.
    /// </summary>
    public abstract class Car
    {
        /// <summary>
        /// Марка.
        /// </summary>
        public string Mark { get; }

        /// <summary>
        /// Пробег.
        /// </summary>
        public int Mileage { get; protected set; }

        /// <summary>
        /// Цена.
        /// </summary>
        public int Price { get; protected set; }

        /// <summary>
        /// Максимальная скорость.
        /// </summary>
        public float MaxSpeed { get; }

        /// <summary>
        /// Настоящая скорость.
        /// </summary>
        public float Speed { get; protected set; }

        /// <summary>
        /// Разгон до 100 км.
        /// </summary>
        public float Overclocking { get; }

        public int exampleField;

        /// <summary>
        /// Инициализатор автомобиля.
        /// </summary>
        /// <param name="mark"> Марка. </param>
        /// <param name="mileage"> Пробег. </param>
        /// <param name="price"> Цена. </param>
        /// <param name="speed"> Скорость. </param>
        /// <param name="overclocking"> Разгон до 100 км. </param>
        public Car(string mark, int mileage, int price, float speed, float overclocking)
        {
            Mark = mark;
            Mileage = mileage;
            Price = price;
            MaxSpeed = speed;        
            Overclocking = overclocking;
            Speed = 0;
        }

        /// <summary>
        /// Ускорение.
        /// </summary>
        /// <param name="speed"> Скорость. </param>
        public void Acceleration(float speed)
        {
            Speed += speed;
        }

        /// <summary>
        /// Характеристики автомобиля.
        /// </summary>
        /// <returns> Характеристики автомобиля. </returns>
        public override string ToString()
        {
            return $"Марка - {Mark}, пробег - {Mileage}, скорость - {MaxSpeed}, цена - {Price}, разгон - {Overclocking}.";
        }
    }
}
