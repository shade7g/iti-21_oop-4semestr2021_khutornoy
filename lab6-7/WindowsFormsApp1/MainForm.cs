﻿using Media;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class MainForm : Form
    {
        Operations operations = new Operations();

        public MainForm()
        {
            InitializeComponent();
            DataLoad();
            Update();
        }

        public void DataLoad()
        {
            addKind.Items.Clear();
            addKind.Items.AddRange(operations.GetKinds());

            names.Items.Clear();
            names.Items.AddRange(operations.GetGetAnimalsNames());

            upAnimalName.Items.Clear();
            upAnimalName.Items.AddRange(operations.GetGetAnimalsNames());

            upKind.Items.Clear();
            upKind.Items.AddRange(operations.GetKinds());
        }

        public void Update()
        {
            dataGridView1.Rows.Clear();
            
                List<string[]> animals = operations.GetAnimals();
                foreach (string[] animal in animals)
                {
                    dataGridView1.Rows.Add(animal);
                }  
            DataLoad();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedKind = addKind.Text;
            addSubSpecies.Items.Clear();
            addSubSpecies.Items.AddRange(operations.GetSubspecies(selectedKind));
        }

        private void add_Click(object sender, EventArgs e)
        {
            string newName = addAnimalName.Text;
            string newKind = addKind.Text;
            string newSubspecies = addSubSpecies.Text;
            string newWeight = addWeight.Value.ToString();
            string newPopulationSize = addPopulationSize.Value.ToString();

            string str = operations.AddAnimal(newName, newKind, newSubspecies, newWeight, newPopulationSize );
            MessageBox.Show(str);

            Update();
        }

        private void remove_Click(object sender, EventArgs e)
        {
            string removeName = names.Text;
            string str = operations.RemoveAnimal(removeName);
            MessageBox.Show(str);
            Update();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string newName = upAnimalName.Text;
            string newKind = upKind.Text;
            string newSubspecies = upSubSpecies.Text;
            string newWeight = upWeight.Value.ToString();
            string newPopulationSize = upPopulationSize.Value.ToString();

            string str = operations.UpdateAnimal(newName, newKind, newSubspecies, newWeight, newPopulationSize);
            MessageBox.Show(str);

            Update();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            DataLoad();
            Update();
        }

        private void upKind_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedKind = addKind.Text;
            upSubSpecies.Items.Clear();
            upSubSpecies.Items.AddRange(operations.GetSubspecies(selectedKind));
        }
    }
}
