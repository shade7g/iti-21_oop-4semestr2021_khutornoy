﻿using ClassLibrary.Object;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

namespace ClassLibrary
{
    /// <summary>
    /// ProductLinq
    /// </summary>
    /// <seealso cref="ClassLibrary.IProduct" />
    public class SubTypesLinq : ISubTypes
    {
        /// <summary>
        /// The data context
        /// </summary>
        private DataContext _dataContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductLinq"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public SubTypesLinq(string connectionString)
        {
            _dataContext = new DataContext(connectionString);
        }

        /// <summary>
        /// Gets the notes.
        /// </summary>
        /// <returns></returns>
        public SubTypes[] GetNotes()
        {
            var objectList = from i in _dataContext.GetTable<SubTypes>() select i;
            List<SubTypes> elements = new List<SubTypes>();
            foreach (SubTypes element in objectList)
            {
                elements.Add(element);
            }
            return elements.ToArray();
        }

        /// <summary>
        /// Gets the note.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public SubTypes GetNote(int id)
        {
            var objectList = from i in _dataContext.GetTable<SubTypes>() where i.SubTypeId == id select i;
            return objectList.First();
        }

        /// <summary>
        /// Gets the note.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        public SubTypes GetNote(SubTypes element) =>  GetNote(element.SubTypeId);

        /// <summary>
        /// Inserts the specified add element.
        /// </summary>
        /// <param name="addElement">The add element.</param>
        /// <returns></returns>
        public bool Insert(SubTypes addElement)
        {
            _dataContext.GetTable<SubTypes>().InsertOnSubmit(addElement);
            _dataContext.SubmitChanges();
            var elements = GetNotes();
            addElement.SubTypeId = elements[elements.Length - 1].SubTypeId;
            return true;
        }

        /// <summary>
        /// Updates the specified edit recepit.
        /// </summary>
        /// <param name="editRecepit">The edit recepit.</param>
        /// <param name="newElement">The new element.</param>
        /// <returns></returns>
        public bool Update(SubTypes editRecepit, SubTypes newElement)
        {
            _dataContext.GetTable<SubTypes>().DeleteOnSubmit(editRecepit);
            _dataContext.GetTable<SubTypes>().InsertOnSubmit(newElement);
            _dataContext.SubmitChanges();
            var elements = GetNotes();
            newElement.SubTypeId = elements[elements.Length - 1].SubTypeId;
            return true;
        }

        /// <summary>
        /// Deletes the specified delete.
        /// </summary>
        /// <param name="delete">The delete.</param>
        /// <returns></returns>
        public bool Delete(SubTypes delete)
        {
            _dataContext.GetTable<SubTypes>().DeleteOnSubmit(delete);
            _dataContext.SubmitChanges();
            return true;
        }
    }
}