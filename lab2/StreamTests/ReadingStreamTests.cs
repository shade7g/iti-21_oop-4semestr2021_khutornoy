﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace StreamDecoration_Library.Tests
{
    /// <summary>
    /// Тестирование класса ReadingStream.
    /// </summary>
    [TestClass()]
    public class ReadingStreamTests
    {
        string path = @"C:\Users\Max\source\repos\iti-21_oop-4semestr2021_khutornoy\lab2\testStream.txt";

        /// <summary>
        /// Тестирование метода GetText.
        /// </summary>
        [TestMethod()]
        public void GetText_Test()
        {
            // Arrang.
            var fs = new FileStream(path, FileMode.Open);
            var rs = new ReadingStream(fs);

            // Act.
            var actual = rs.GetText();
            var expected = File.ReadAllText(path);

            // Assert.
            Assert.AreEqual(actual, expected);
        }
    }
}