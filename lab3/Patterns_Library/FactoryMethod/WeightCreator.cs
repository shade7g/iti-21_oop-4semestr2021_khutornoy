﻿using System;

namespace Patterns_Library
{
    /// <summary>
    /// Создаем конкретный объект фабрики AddWeight.
    /// </summary>
    public class WeightCreator : NewManCreator
    {
        public override ManFactory CreateNewMan()
        {
            return new AddWeight();
        }
    }
}
