﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace StreamDecoration_Library
{
    /// <summary>
    /// Декарированный класс Stream.
    /// </summary>
    public class ReadingStream : Stream
    {
        private Stream stream;
        private double runTime;

        public ReadingStream(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("Передаваемый объект типа Stream является пустым!");
            }

            this.stream = stream;
            runTime = 0;
        }

        public override Boolean CanRead => stream.CanRead;

        public override Boolean CanSeek => stream.CanSeek;

        public override Boolean CanWrite => stream.CanWrite;

        public override long Length => stream.Length;

        public override long Position { get => stream.Position; set => stream.Position = value; }

        public override void Flush()
        {
            stream.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return stream.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return stream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            stream.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            stream.Write(buffer, offset, count);
        }      

        /// <summary>
        /// Время работы с потоком.
        /// </summary>
        public double RunTime
        {
            get { return runTime; }
        }

        /// <summary>
        /// Считывание байтов из текущего потока.
        /// </summary>
        /// <returns> Массив байтов. </returns>
        private byte[] ByteReader()
        {          
            if (CanRead)
            {
                var ts = new RunTimeStream();

                ts.Start();
                          
                var buffer = new byte[stream.Length];
                Read(buffer, 0, buffer.Length);
               
                stream.Dispose();

                runTime = ts.Stop();

                return buffer;
            }

            stream.Dispose();

            return null;
        }

        /// <summary>
        /// Считывание текста из текущего потока.
        /// </summary>
        /// <returns> Весь текст файла. </returns>
        public string GetText()
        {
            var buffer = ByteReader();

            return Encoding.UTF8.GetString(buffer);
        }

    }
}
