﻿using ClassLibrary.Object;

namespace ClassLibrary
{
    /// <summary>
    /// IMainShop
    /// </summary>
    public interface IAnimals
    {
        /// <summary>
        /// Gets the notes.
        /// </summary>
        /// <returns></returns>
        Animals[] GetNotes();

        /// <summary>
        /// Gets the note.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Animals GetNote(int id);

        /// <summary>
        /// Gets the note.
        /// </summary>
        /// <param name="servicez">The servicez.</param>
        /// <returns></returns>
        Animals GetNote(Animals servicez);

        /// <summary>
        /// Inserts the specified add servicez.
        /// </summary>
        /// <param name="addServicez">The add servicez.</param>
        /// <returns></returns>
        bool Insert(Animals addServicez);

        /// <summary>
        /// Updates the specified edit servicez.
        /// </summary>
        /// <param name="editServicez">The edit servicez.</param>
        /// <param name="newServicez">The new servicez.</param>
        /// <returns></returns>
        bool Update(Animals editServicez, Animals newServicez);

        /// <summary>
        /// Deletes the specified delete servicez.
        /// </summary>
        /// <param name="deleteServicez">The delete servicez.</param>
        /// <returns></returns>
        bool Delete(Animals deleteServicez);
    }
}