﻿
namespace Circle_App
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ColorChange_Button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.DrawCircle_Button = new System.Windows.Forms.Button();
            this.CircleSize_Box = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.HorizontalMove_Box = new System.Windows.Forms.CheckBox();
            this.VertivalMove_Box = new System.Windows.Forms.CheckBox();
            this.ChangeColor_Panel = new System.Windows.Forms.ColorDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainPanel
            // 
            this.MainPanel.BackColor = System.Drawing.Color.White;
            this.MainPanel.Location = new System.Drawing.Point(12, 165);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(1230, 540);
            this.MainPanel.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkViolet;
            this.panel1.Controls.Add(this.ColorChange_Button);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.DrawCircle_Button);
            this.panel1.Controls.Add(this.CircleSize_Box);
            this.panel1.Location = new System.Drawing.Point(66, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(388, 135);
            this.panel1.TabIndex = 2;
            // 
            // ColorChange_Button
            // 
            this.ColorChange_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ColorChange_Button.Location = new System.Drawing.Point(218, 27);
            this.ColorChange_Button.Name = "ColorChange_Button";
            this.ColorChange_Button.Size = new System.Drawing.Size(151, 32);
            this.ColorChange_Button.TabIndex = 5;
            this.ColorChange_Button.Text = "Выбрать цвет";
            this.ColorChange_Button.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(60, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Размер";
            // 
            // DrawCircle_Button
            // 
            this.DrawCircle_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DrawCircle_Button.Location = new System.Drawing.Point(79, 78);
            this.DrawCircle_Button.Name = "DrawCircle_Button";
            this.DrawCircle_Button.Size = new System.Drawing.Size(241, 43);
            this.DrawCircle_Button.TabIndex = 2;
            this.DrawCircle_Button.Text = "Нарисовать круг";
            this.DrawCircle_Button.UseVisualStyleBackColor = true;
            // 
            // CircleSize_Box
            // 
            this.CircleSize_Box.Location = new System.Drawing.Point(46, 37);
            this.CircleSize_Box.Name = "CircleSize_Box";
            this.CircleSize_Box.Size = new System.Drawing.Size(100, 22);
            this.CircleSize_Box.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkViolet;
            this.panel2.Controls.Add(this.HorizontalMove_Box);
            this.panel2.Controls.Add(this.VertivalMove_Box);
            this.panel2.Location = new System.Drawing.Point(581, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(280, 135);
            this.panel2.TabIndex = 3;
            // 
            // HorizontalMove_Box
            // 
            this.HorizontalMove_Box.AutoSize = true;
            this.HorizontalMove_Box.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.HorizontalMove_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HorizontalMove_Box.Location = new System.Drawing.Point(9, 78);
            this.HorizontalMove_Box.Name = "HorizontalMove_Box";
            this.HorizontalMove_Box.Size = new System.Drawing.Size(253, 24);
            this.HorizontalMove_Box.TabIndex = 1;
            this.HorizontalMove_Box.Text = "Движение по горизонтали";
            this.HorizontalMove_Box.UseVisualStyleBackColor = true;
            // 
            // VertivalMove_Box
            // 
            this.VertivalMove_Box.AutoSize = true;
            this.VertivalMove_Box.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.VertivalMove_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.VertivalMove_Box.Location = new System.Drawing.Point(9, 32);
            this.VertivalMove_Box.Name = "VertivalMove_Box";
            this.VertivalMove_Box.Size = new System.Drawing.Size(235, 24);
            this.VertivalMove_Box.TabIndex = 0;
            this.VertivalMove_Box.Text = "Движение по вертикали";
            this.VertivalMove_Box.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumOrchid;
            this.ClientSize = new System.Drawing.Size(1287, 717);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.MainPanel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel MainPanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button ColorChange_Button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button DrawCircle_Button;
        private System.Windows.Forms.TextBox CircleSize_Box;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox HorizontalMove_Box;
        private System.Windows.Forms.CheckBox VertivalMove_Box;
        private System.Windows.Forms.ColorDialog ChangeColor_Panel;
    }
}

